#Imports
from keras.layers import Input
from keras.layers import Conv3D
from keras.layers import MaxPool3D, GlobalMaxPooling3D
from keras.layers import Dense
from keras.layers import AveragePooling3D
from keras.layers import Flatten
from keras.layers import Activation, Subtract
from keras.layers import BatchNormalization
from keras.layers import Dropout
from keras.models import Model
from keras.regularizers import l2
import keras.backend as K
from keras import regularizers

from deep_learning.blocks import *
from deep_learning.base_networks import *
from deep_learning.parameters import *

import numpy as np

########### MISH ACTIVATION ######
def mish(x):
    return x*K.tanh(K.softplus(x))

#Residual block
def residualAttentionNetwork(shape=(45, 54, 45, 1), n_channels=32, n_classes=2, nLayersForFeatExtr=1):
    """
    Attention-56 ResNet for Cifar10 Dataset
    https://arxiv.org/abs/1704.06904
    """
    input_ = Input(shape=shape)
    
    x = input_
    
    #Loop through number of layers
    while(nLayersForFeatExtr > 0):
        
        #Convolution blocks
        x = Conv3D(n_channels, (2, 2, 2), padding='same')(x)
        x = BatchNormalization()(x)
        x = Activation('relu')(x)
        
        #Test 102/103
#         x = MaxPool3D(pool_size=(2, 2, 2))(x) 

        #Residual + attention blocks
        x = residual_block(x, input_channels=n_channels, output_channels=16)
        x = attention_block(x, encoder_depth=1)

        x = residual_block(x, input_channels=n_channels*4, output_channels=32, stride=2) 
        x = attention_block(x, encoder_depth=1)

        x = residual_block(x, input_channels=n_channels*8, output_channels=64, stride=2)
        x = attention_block(x, encoder_depth=1)

#         Residual blocks
        x = residual_block(x, input_channels=n_channels*8, output_channels=n_channels*16)
    
        #Test 107 (remove for this test)
        x = residual_block(x, input_channels=n_channels*16, output_channels=n_channels*16)
        
        #Test 106 / 107 (remove for this test)
        x = residual_block(x, input_channels=n_channels*16, output_channels=n_channels*16)

        #Test 103
#         x = AveragePooling3D(pool_size=(2, 2, 2), strides=(1, 1, 1))(x)  # 1x1
        
        nLayersForFeatExtr -= 1

    #Return model
    model = Model(input_, x)
    return model

#Residual block
def residualAttentionNetwork_semiTrainable(shape=(45, 54, 45, 1), n_channels=32, n_classes=2, nLayersForFeatExtr=1):
    """
    Attention-56 ResNet for Cifar10 Dataset
    https://arxiv.org/abs/1704.06904
    """
    input_ = Input(shape=shape)
    
    x = input_
    
    #Loop through number of layers
    while(nLayersForFeatExtr > 0):
        
        #Convolution blocks
        x = Conv3D(n_channels, (2, 2, 2), padding='same', trainable=False)(x)
        x = BatchNormalization()(x)
        x = Activation('relu')(x)
        
        #Test 102/103
#         x = MaxPool3D(pool_size=(2, 2, 2))(x) 

        #Residual + attention blocks
        x = residual_block_semiTrainable(x, input_channels=n_channels, output_channels=16)
        x = attention_block_semiTrainable(x, encoder_depth=1)

        x = residual_block_semiTrainable(x, input_channels=n_channels*4, output_channels=32, stride=2) 
        x = attention_block_semiTrainable(x, encoder_depth=1)

        x = residual_block_semiTrainable(x, input_channels=n_channels*8, output_channels=64, stride=2)
        x = attention_block_semiTrainable(x, encoder_depth=1)

#         Residual blocks
        x = residual_block_semiTrainable(x, input_channels=n_channels*8, output_channels=n_channels*16)
    
        #Test 107 (remove for this test)
        x = residual_block_semiTrainable(x, input_channels=n_channels*16, output_channels=n_channels*16)
        
        #Test 106 / 107 (remove for this test)
        x = residual_block_semiTrainable(x, input_channels=n_channels*16, output_channels=n_channels*16)

        #Test 103
#         x = AveragePooling3D(pool_size=(2, 2, 2), strides=(1, 1, 1))(x)  # 1x1
        
        nLayersForFeatExtr -= 1

    #Return model
    model = Model(input_, x)
    return model

def symmetricResidualAttentionNetwork(shape=(45, 54, 45, 1), n_channels=32, n_classes=2, nLayersForFeatExtr=1):
    
    #Residual network
    shared_network = residualAttentionNetwork(shape=shape, 
                                              n_channels=n_channels, 
                                              n_classes=n_classes, 
                                              nLayersForFeatExtr=nLayersForFeatExtr)
    
    #Two brain inputs
    input_left = Input(shape=(shape))
    input_right = Input(shape=(shape))
    
    # Re-use same instance of base network (weights are shared across both branches)
    processed_left = shared_network(input_left)
    processed_right = shared_network(input_right)
    
    # Merging features from two hemispheres
    # l1
    l1_distance_layer = Lambda(lambda tensors: K.abs(tensors[0] - tensors[1]))
    merged_layer = l1_distance_layer([processed_left, processed_right])
    
    # Modules after L-1 merge
    x = Conv3D(n_channels, (3, 3, 3), padding='same')(merged_layer)
    x = BatchNormalization()(x)
    x = Activation('relu')(x)
    x = MaxPool3D(pool_size=(2, 2, 2))(x)  # 16x16

    x = residual_block(x, input_channels=n_channels, output_channels=64)
#     x = attention_block(x, encoder_depth=2)
#     x = attention_block(x, encoder_depth=1)

#     x = residual_block(x, input_channels=n_channels*4, output_channels=256, stride=2)  # 8x8
#     x = attention_block(x, encoder_depth=1)

#     x = residual_block(x, input_channels=n_channels*8, output_channels=512, stride=2)  # 4x4
#     x = attention_block(x, encoder_depth=1)

#     x = residual_block(x, input_channels=n_channels*16, output_channels=n_channels*32)
#     x = residual_block(x, input_channels=n_channels*32, output_channels=n_channels*32)
#     x = residual_block(x, input_channels=n_channels*32, output_channels=n_channels*32)

#     x = AveragePooling3D(pool_size=(4, 4, 4), strides=(1, 1, 1))(x)  # 1x1
    
    #Flatten and output
    x = Flatten()(x)
    output = Dense(n_classes, activation='softmax')(x)

    model = Model([input_left, input_right], output)
    
    return model

def symmetric_ResidualAttention_Inception(input_shape,
                                        depth=2,
                                        n_inception=1, # # inception modules after merge
                                        activation='relu',
                                        use_dropout=False,
                                        use_pooling_before_merge=False,
                                        use_pooling_after_merge=False,
                                        use_final_pooling=True,
                                        n_classes=2,
                                        kernel_regularizer = None,
                                        activity_regularizer = None,
                                        n_channels=8,
                                        nLayersForFeatExtr=1):
    
    #Input
    input_left = Input(shape=(input_shape))
    input_right = Input(shape=(input_shape))    
    
    
    ####################### ATTENTION ########################################
    attn_network = residualAttentionNetwork(shape=input_shape, 
                                              n_channels=n_channels, 
                                              n_classes=n_classes, 
                                              nLayersForFeatExtr=nLayersForFeatExtr)
    
    # Re-use same instance of base network (weights are shared across both branches)
    processed_left_attn = attn_network(input_left)
    processed_right_attn = attn_network(input_right)
    
    # Merging features from two hemispheres
    # l1
    l1_distance_layer = Lambda(lambda tensors: K.abs(tensors[0] - tensors[1]))
    merged_layer = l1_distance_layer([processed_left_attn, processed_right_attn])
    
    # Modules after L-1 merge
    x = Conv3D(n_channels, (3, 3, 3), padding='same')(merged_layer)
    x = BatchNormalization()(x)
    x = Activation('relu')(x)
    x = MaxPool3D(pool_size=(2, 2, 2))(x)  # 16x16

    x = residual_block(x, input_channels=n_channels, output_channels=64)
    
    x = Flatten()(x)
    

    ####################### INCEPTION ########################################
    base_network = inception_CNN_2_5d(input_shape,
                                    depth=depth,
                                    activation=activation,
                                    use_dropout=use_dropout,
                                    use_pooling=use_pooling_before_merge,
                                    kernel_regularizer = kernel_regularizer,
                                    activity_regularizer = activity_regularizer)



    # Re-use same instance of base network (weights are shared across both branches)
    processed_left = base_network(input_left)
    processed_right = base_network(input_right)
    
    # Maxpooling
    if use_pooling_before_merge:
        processed_left = MaxPooling3D(strides=(2, 2, 2), padding='same')(processed_left)
        processed_right = MaxPooling3D(strides=(2, 2, 2), padding='same')(processed_right)

    
    # Merging features from two hemispheres
    # l1
    l1_distance_layer = Lambda(lambda tensors: K.abs(tensors[0] - tensors[1]))
    merged_layer = l1_distance_layer([processed_left, processed_right])
    
    filters = 2

    # Inception module
    tower_1x1 = Conv3D(filters, kernel_size=(1, 1, 1), padding='same', activation=activation)(merged_layer)
    tower_1 = Conv3D(filters, kernel_size=(5, 5, 5), padding='same', activation=activation)(merged_layer)

#     tower_2 = Conv3D(filters, kernel_size=(1, 1, 1), padding='same', activation=activation)(merged_layer)
    tower_2 = Conv3D(filters, kernel_size=(5, 1, 5), padding='same', activation=activation)(merged_layer)

    tower_3 = MaxPooling3D((3, 3, 3), strides=(1, 1, 1), padding='same')(merged_layer)
    tower_3 = Conv3D(filters, kernel_size=(1, 1, 1), padding='same', activation=activation)(tower_3)

#     tower_4 = Conv3D(filters, kernel_size=(1, 1, 1), padding='same', activation=activation)(merged_layer)
    
#     tower_5 = Conv3D(filters, kernel_size=(1, 1, 1), padding='same', activation=activation)(merged_layer)
    tower_5 = Conv3D(filters, kernel_size=(5, 5, 1), padding='same', activation=activation)(merged_layer)

    merged_layer = Concatenate(axis=4)([tower_1, tower_3, tower_1x1])

    
    if use_dropout:
        merged_layer = Dropout(0.1)(merged_layer)

    # Maxpooling
    if use_final_pooling:
        merged_layer = MaxPooling3D(strides=(2, 2, 2), padding='same')(merged_layer)

    # Flatten
    merged_layer = Flatten()(merged_layer)
    merged_layer = Concatenate()([merged_layer, x])

    # Prediction layer
    prediction = Dense(units=n_classes, activation='softmax')(merged_layer)


    return Model(inputs=[input_left, input_right], outputs=prediction)


def symmetric_ResidualAttention_Inception_subtract(input_shape,
                                        depth=2,
                                        n_inception=1, # # inception modules after merge
                                        activation='relu',
                                        use_dropout=False,
                                        use_pooling_before_merge=False,
                                        use_pooling_after_merge=False,
                                        use_final_pooling=True,
                                        n_classes=2,
                                        kernel_regularizer = None,
                                        activity_regularizer = None,
                                        n_channels=8,
                                        nLayersForFeatExtr=1):
    
    #Input
    input_left = Input(shape=(input_shape))
    input_right = Input(shape=(input_shape))    
    
    
    ####################### ATTENTION ########################################
    attn_network = residualAttentionNetwork(shape=input_shape, 
                                              n_channels=n_channels, 
                                              n_classes=n_classes, 
                                              nLayersForFeatExtr=nLayersForFeatExtr)
    
    # Re-use same instance of base network (weights are shared across both branches)
    processed_left_attn = attn_network(input_left)
    processed_right_attn = attn_network(input_right)
    
    
    merged_layer = Subtract()([processed_left_attn, processed_right_attn])

    # Modules after L-1 merge
    x = Conv3D(n_channels, (3, 3, 3), padding='same')(merged_layer)
    x = BatchNormalization()(x)
    x = Activation('relu')(x)
    x = MaxPool3D(pool_size=(2, 2, 2))(x) 

    x = residual_block(x, input_channels=n_channels, output_channels=64)
    
    x = Flatten()(x)
    

    ####################### INCEPTION ########################################
    base_network = inception_CNN_2_5d(input_shape,
                                    depth=depth,
                                    activation=activation,
                                    use_dropout=use_dropout,
                                    use_pooling=use_pooling_before_merge,
                                    kernel_regularizer = kernel_regularizer,
                                    activity_regularizer = activity_regularizer)



    # Re-use same instance of base network (weights are shared across both branches)
    processed_left = base_network(input_left)
    processed_right = base_network(input_right)
    
    # Maxpooling
    if use_pooling_before_merge:
        processed_left = MaxPooling3D(strides=(2, 2, 2), padding='same')(processed_left)
        processed_right = MaxPooling3D(strides=(2, 2, 2), padding='same')(processed_right)

    #Subtract
    merged_layer = Subtract()([processed_left, processed_right])
    
    filters = 2

#     kernel_regularizer = regularizers.l1_l2(l1=0.001, l2=0.001)
    kernel_regularizer = None
    
    # Inception module
    tower_1 = Conv3D(filters, kernel_size=(1, 1, 1), padding='same', activation=activation, kernel_regularizer=kernel_regularizer)(merged_layer)
    tower_2 = Conv3D(filters, kernel_size=(5, 5, 5), padding='same', activation=activation, kernel_regularizer=kernel_regularizer)(merged_layer)

    tower_3 = Conv3D(filters, kernel_size=(5, 5, 1), padding='same', activation=activation, kernel_regularizer=kernel_regularizer)(merged_layer)
    
    tower_4 = MaxPooling3D((3, 3, 3), strides=(1, 1, 1), padding='same')(merged_layer)
    tower_4 = Conv3D(filters, kernel_size=(1, 1, 1), padding='same', activation=activation, kernel_regularizer=kernel_regularizer)(tower_4)


    merged_layer = Concatenate(axis=4)([tower_1, tower_2, tower_4])

    
    if use_dropout:
        merged_layer = Dropout(0.05)(merged_layer)

    # Maxpooling
    if use_final_pooling:
        merged_layer = MaxPooling3D(strides=(2, 2, 2), padding='same')(merged_layer)

    # Flatten
    merged_layer = Flatten()(merged_layer)
    merged_layer = Concatenate()([merged_layer, x])
    
    
    #Dropout
#     merged_layer = Dropout(0.05)(merged_layer)
    
    # Prediction layer
    prediction = Dense(units=n_classes, activation='softmax')(merged_layer)


    return Model(inputs=[input_left, input_right], outputs=prediction)

def symmetric_ResidualAttention_Attention_subtract(input_shape,
                                        depth=2,
                                        n_inception=1, # # inception modules after merge
                                        activation='relu',
                                        use_dropout=False,
                                        use_pooling_before_merge=False,
                                        use_pooling_after_merge=False,
                                        use_final_pooling=True,
                                        n_classes=2,
                                        kernel_regularizer = None,
                                        activity_regularizer = None,
                                        n_channels=8,
                                        nLayersForFeatExtr=1):
    
    #Input
    input_left = Input(shape=(input_shape))
    input_right = Input(shape=(input_shape))    
    
    
    ####################### ATTENTION 1 ########################################
    attn_network1 = residualAttentionNetwork(shape=input_shape, 
                                              n_channels=n_channels, 
                                              n_classes=n_classes, 
                                              nLayersForFeatExtr=nLayersForFeatExtr)
    
    # Re-use same instance of base network (weights are shared across both branches)
    processed_left_attn1 = attn_network1(input_left)
    processed_right_attn1 = attn_network1(input_right)
    
    
    merged_layer1 = Subtract()([processed_left_attn1, processed_right_attn1])

    # Modules after L-1 merge
    x1 = Conv3D(n_channels, (3, 3, 3), padding='same')(merged_layer1)
    x1 = BatchNormalization()(x1)
    x1 = Activation('relu')(x1)
    x1 = MaxPool3D(pool_size=(3, 3, 3))(merged_layer1)  # 16x16

    x1 = residual_block(x1, input_channels=n_channels, output_channels=32) 
    
    x1 = Flatten()(x1)
    
    ####################### ATTENTION 2 ########################################
    attn_network2 = residualAttentionNetwork(shape=input_shape, 
                                              n_channels=n_channels, 
                                              n_classes=n_classes, 
                                              nLayersForFeatExtr=nLayersForFeatExtr)
    
    # Re-use same instance of base network (weights are shared across both branches)
    processed_left_attn2 = attn_network2(input_left)
    processed_right_attn2 = attn_network2(input_right)
    
    
    merged_layer2 = Subtract()([processed_left_attn2, processed_right_attn2])

    # Modules after L-1 merge
    x2 = Conv3D(n_channels, (3, 3, 3), padding='same')(merged_layer2)
    x2 = BatchNormalization()(x2)
    x2 = Activation('relu')(x2)
    x2 = MaxPool3D(pool_size=(3, 3, 3))(merged_layer2)  # 16x16

    x2 = residual_block(x2, input_channels=n_channels, output_channels=64)
    
    x2 = Flatten()(x2)
    
    #Concat
    merged_layer = Concatenate()([x1, x2])
    
    
    #Dropout
#     merged_layer = Dropout(0.05)(merged_layer)
    
    # Prediction layer
    prediction = Dense(units=n_classes, activation='softmax')(merged_layer)


    return Model(inputs=[input_left, input_right], outputs=prediction)


def symmetric_Inception_noSubtract(input_shape,
                                        depth=2,
                                        n_inception=1, # # inception modules after merge
                                        activation='relu',
                                        use_dropout=False,
                                        use_pooling_before_merge=False,
                                        use_pooling_after_merge=False,
                                        use_final_pooling=True,
                                        n_classes=2,
                                        kernel_regularizer = None,
                                        activity_regularizer = None,
                                        n_channels=8,
                                        nLayersForFeatExtr=1):
    
    #Input
    input_left = Input(shape=(input_shape))
    input_right = Input(shape=(input_shape))    

    ####################### INCEPTION ########################################
    base_network = inception_CNN_2_5d(input_shape,
                                    depth=depth,
                                    activation=activation,
                                    use_dropout=use_dropout,
                                    use_pooling=use_pooling_before_merge,
                                    kernel_regularizer = kernel_regularizer,
                                    activity_regularizer = activity_regularizer)



    # Re-use same instance of base network (weights are shared across both branches)
    processed_left = base_network(input_left)
    processed_right = base_network(input_right)
    
    #Max pooling
#     processed_left = MaxPooling3D(strides=(2, 2, 2), padding='same')(processed_left)
#     processed_right = MaxPooling3D(strides=(2, 2, 2), padding='same')(processed_right)
        
    merged_layer = Concatenate()([processed_left, processed_right])
        
    # Modules after processing
    merged_layer = Conv3D(n_channels, (3, 3, 3), padding='same')(merged_layer)
#     merged_layer = Activation('relu')(merged_layer)
#     merged_layer = BatchNormalization()(merged_layer)
    merged_layer = MaxPool3D(pool_size=(2, 2, 2))(merged_layer) 
    merged_layer = Flatten()(merged_layer)

    
    # Prediction layer
    prediction = Dense(units=n_classes, activation='softmax')(merged_layer)


    return Model(inputs=[input_left, input_right], outputs=prediction)

def symmetric_Inception_Subtract_reduceDim(input_shape,
                                        depth=2,
                                        n_inception=1, # # inception modules after merge
                                        activation='relu',
                                        use_dropout=False,
                                        use_pooling_before_merge=False,
                                        use_pooling_after_merge=False,
                                        use_final_pooling=True,
                                        n_classes=2,
                                        kernel_regularizer = None,
                                        activity_regularizer = None,
                                        n_channels=8,
                                        nLayersForFeatExtr=1):
    
    #Input
    input_left = Input(shape=(input_shape))
    input_right = Input(shape=(input_shape))    

   ####################### INCEPTION ########################################
    base_network = inception_CNN_2_5d(input_shape,
                                    depth=depth,
                                    activation=activation,
                                    use_dropout=use_dropout,
                                    use_pooling=use_pooling_before_merge,
                                    kernel_regularizer = kernel_regularizer,
                                    activity_regularizer = activity_regularizer)



    # Re-use same instance of base network (weights are shared across both branches)
    processed_left = base_network(input_left)
    processed_right = base_network(input_right)
    
    # Maxpooling
    if use_pooling_before_merge:
        processed_left = MaxPooling3D(strides=(2, 2, 2), padding='same')(processed_left)
        processed_right = MaxPooling3D(strides=(2, 2, 2), padding='same')(processed_right)

    #Subtract
    merged_layer = Subtract()([processed_left, processed_right])
    
    #Concatenate previous images
#     merged_layer = Concatenate()([merged_layer, processed_left, processed_right])
    
    filters = 8
#     kernel_regularizer = regularizers.l1_l2(l1=0.001, l2=0.001)
    kernel_regularizer = None
    
    # Inception module
    tower_1 = Conv3D(filters, kernel_size=(1, 1, 1), padding='same', activation=activation, kernel_regularizer=kernel_regularizer)(merged_layer)
    tower_2 = Conv3D(filters, kernel_size=(5, 5, 5), padding='same', activation=activation, kernel_regularizer=kernel_regularizer)(merged_layer)

    tower_3 = Conv3D(filters, kernel_size=(5, 5, 1), padding='same', activation=activation, kernel_regularizer=kernel_regularizer)(merged_layer)
    
    tower_4 = MaxPooling3D((3, 3, 3), strides=(1, 1, 1), padding='same')(merged_layer)
    tower_4 = Conv3D(filters, kernel_size=(1, 1, 1), padding='same', activation=activation, kernel_regularizer=kernel_regularizer)(tower_4)


    merged_layer = Concatenate(axis=4)([tower_1, tower_2, tower_4])

    
    if use_dropout:
        merged_layer = Dropout(0.05)(merged_layer)

    # Maxpooling
    if use_final_pooling:
        #Original
#         merged_layer = MaxPooling3D(strides=(2, 2, 2), padding='same')(merged_layer)

#         #Test 87
#         merged_layer = Conv3D(1, kernel_size=(1, 1, 1), padding='same', activation=activation, kernel_regularizer=kernel_regularizer)(merged_layer)
#         merged_layer = MaxPooling3D(strides=(10, 10, 1), padding='same')(merged_layer)
        
#         #Test 88
#         merged_layer = Conv3D(1, kernel_size=(1, 1, 1), padding='same', activation=activation, kernel_regularizer=kernel_regularizer)(merged_layer)
#         merged_layer = MaxPooling3D(strides=(4, 4, 4), padding='same')(merged_layer)
        
        #Test 89
        merged_layer = MaxPooling3D(strides=(4, 4, 4), padding='same')(merged_layer)

    # Flatten
    merged_layer = Flatten()(merged_layer)
#     merged_layer = Concatenate()([merged_layer, x])
    
    
    #Dropout
#     merged_layer = Dropout(0.05)(merged_layer)
    
    # Prediction layer
    prediction = Dense(units=n_classes, activation='softmax')(merged_layer)


    return Model(inputs=[input_left, input_right], outputs=prediction)

def symmetric_Inception_3session(input_shape,
                                        depth=2,
                                        n_inception=1, # # inception modules after merge
                                        activation='relu',
                                        use_dropout=False,
                                        use_pooling_before_merge=False,
                                        use_pooling_after_merge=False,
                                        use_final_pooling=True,
                                        n_classes=2,
                                        kernel_regularizer = None,
                                        activity_regularizer = None,
                                        n_channels=8,
                                        nLayersForFeatExtr=1):
    
    #Input
    input_first = Input(shape=(input_shape))
    input_second = Input(shape=(input_shape))    
    input_third = Input(shape=(input_shape))    

    ####################### INCEPTION ########################################
    base_network = inception_CNN_2_5d(input_shape,
                                    depth=depth,
                                    activation=activation,
                                    use_dropout=use_dropout,
                                    use_pooling=use_pooling_before_merge,
                                    kernel_regularizer = kernel_regularizer,
                                    activity_regularizer = activity_regularizer)



    # Re-use same instance of base network (weights are shared across both branches)
    processed_first = base_network(input_first)
    processed_second = base_network(input_second)
    processed_third = base_network(input_third)
    
    #Max pooling
#     processed_left = MaxPooling3D(strides=(2, 2, 2), padding='same')(processed_left)
#     processed_right = MaxPooling3D(strides=(2, 2, 2), padding='same')(processed_right)
        
    merged_layer = Concatenate()([processed_first, processed_second, processed_third])
        
    # Modules after processing
    merged_layer = Conv3D(n_channels, (3, 3, 3), padding='same')(merged_layer)
#     merged_layer = Activation('relu')(merged_layer)
#     merged_layer = BatchNormalization()(merged_layer)
    merged_layer = MaxPool3D(pool_size=(2, 2, 2))(merged_layer) 
    merged_layer = Flatten()(merged_layer)

    
    # Prediction layer
    prediction = Dense(units=n_classes, activation='softmax')(merged_layer)


    return Model(inputs=[input_first, input_second, input_third], outputs=prediction)

#2 separate inception networks
def symmetric_Inception_separate(input_shape,
                                        depth=2,
                                        n_inception=1, # # inception modules after merge
                                        activation='relu',
                                        use_dropout=False,
                                        use_pooling_before_merge=False,
                                        use_pooling_after_merge=False,
                                        use_final_pooling=True,
                                        n_classes=2,
                                        kernel_regularizer = None,
                                        activity_regularizer = None,
                                        n_channels=8,
                                        nLayersForFeatExtr=1):
    
    #Input
    input_left = Input(shape=(input_shape))
    input_right = Input(shape=(input_shape))    

    ####################### INCEPTION ########################################
    base_network1 = inception_CNN_2_5d(input_shape,
                                    depth=depth,
                                    activation=activation,
                                    use_dropout=use_dropout,
                                    use_pooling=use_pooling_before_merge,
                                    kernel_regularizer = kernel_regularizer,
                                    activity_regularizer = activity_regularizer)
    
    base_network2 = inception_CNN_2_5d(input_shape,
                                    depth=depth,
                                    activation=activation,
                                    use_dropout=use_dropout,
                                    use_pooling=use_pooling_before_merge,
                                    kernel_regularizer = kernel_regularizer,
                                    activity_regularizer = activity_regularizer)




    # Re-use same instance of base network (weights are shared across both branches)
    processed_left = base_network1(input_left)
    processed_right = base_network2(input_right)
    

    merged_layer = Concatenate()([processed_left, processed_right])
        
    # Modules after processing
    merged_layer = Conv3D(n_channels, (3, 3, 3), padding='same')(merged_layer)
#     merged_layer = Activation('relu')(merged_layer)
#     merged_layer = BatchNormalization()(merged_layer)
    merged_layer = MaxPool3D(pool_size=(2, 2, 2))(merged_layer) 
    merged_layer = Flatten()(merged_layer)

    
    # Prediction layer
    prediction = Dense(units=n_classes, activation='softmax')(merged_layer)


    return Model(inputs=[input_left, input_right], outputs=prediction)

#2 separate inception networks
def symmetric_Inception_separate_subtract(input_shape,
                                        depth=2,
                                        n_inception=1, # # inception modules after merge
                                        activation='relu',
                                        use_dropout=False,
                                        use_pooling_before_merge=False,
                                        use_pooling_after_merge=False,
                                        use_final_pooling=True,
                                        n_classes=2,
                                        kernel_regularizer = None,
                                        activity_regularizer = None,
                                        n_channels=8,
                                        nLayersForFeatExtr=1):
    
    #Input
    input_left = Input(shape=(input_shape))
    input_right = Input(shape=(input_shape))    

    ####################### INCEPTION ########################################
    base_network1 = inception_CNN_2_5d(input_shape,
                                    depth=depth,
                                    activation=activation,
                                    use_dropout=use_dropout,
                                    use_pooling=use_pooling_before_merge,
                                    kernel_regularizer = kernel_regularizer,
                                    activity_regularizer = activity_regularizer)
    
    base_network2 = inception_CNN_2_5d(input_shape,
                                    depth=depth,
                                    activation=activation,
                                    use_dropout=use_dropout,
                                    use_pooling=use_pooling_before_merge,
                                    kernel_regularizer = kernel_regularizer,
                                    activity_regularizer = activity_regularizer)
    




    # Re-use same instance of base network (weights are shared across both branches)
    processed_left = base_network1(input_left)
    processed_right = base_network2(input_right)
    
    
    merged_layer = Subtract()([processed_left, processed_right])
    
    filters = 4
    # Inception module
    tower_1 = Conv3D(filters, kernel_size=(1, 1, 1), padding='same', activation=activation, kernel_regularizer=kernel_regularizer, activity_regularizer=activity_regularizer, kernel_initializer='random_uniform')(merged_layer)
    tower_2 = Conv3D(filters, kernel_size=(3, 3, 3), padding='same', activation=activation, kernel_regularizer=kernel_regularizer, activity_regularizer=activity_regularizer, kernel_initializer='random_uniform')(merged_layer)

    tower_3 = Conv3D(filters, kernel_size=(5, 5, 1), padding='same', activation=activation, kernel_regularizer=kernel_regularizer, activity_regularizer=activity_regularizer, kernel_initializer='random_uniform')(merged_layer)

    tower_4 = MaxPooling3D((3, 3, 3), strides=(1, 1, 1), padding='same')(merged_layer)
    tower_4 = Conv3D(filters, kernel_size=(1, 1, 1), padding='same', activation=activation, kernel_regularizer=kernel_regularizer, activity_regularizer=activity_regularizer, kernel_initializer='random_uniform')(tower_4)


    merged_layer = Concatenate(axis=4)([tower_1, tower_2, tower_4])

    
    merged_layer = MaxPool3D(pool_size=(2, 2, 2))(merged_layer) 
    
    merged_layer = Flatten()(merged_layer)
    
    # Prediction layer
    prediction = Dense(units=n_classes, activation='softmax')(merged_layer)


    return Model(inputs=[input_left, input_right], outputs=prediction)

def symmetric_ResidualAttention_Inception_subtract_attn_end(input_shape,
                                        depth=2,
                                        n_inception=1, # # inception modules after merge
                                        activation='relu',
                                        use_dropout=False,
                                        use_pooling_before_merge=False,
                                        use_pooling_after_merge=False,
                                        use_final_pooling=True,
                                        n_classes=2,
                                        kernel_regularizer = None,
                                        activity_regularizer = None,
                                        n_channels=8,
                                        nLayersForFeatExtr=1):
    
    #Input
    input_left = Input(shape=(input_shape))
    input_right = Input(shape=(input_shape))    
    
    
    ####################### ATTENTION ########################################
    attn_network = residualAttentionNetwork(shape=input_shape, 
                                              n_channels=n_channels, 
                                              n_classes=n_classes, 
                                              nLayersForFeatExtr=nLayersForFeatExtr)
    
    # Re-use same instance of base network (weights are shared across both branches)
    processed_left_attn = attn_network(input_left)
    processed_right_attn = attn_network(input_right)
    
    
    merged_layer = Subtract()([processed_left_attn, processed_right_attn])

#     # Modules after L-1 merge
#     x = Conv3D(n_channels, (3, 3, 3), padding='same')(merged_layer)
#     x = BatchNormalization()(x)
#     x = Activation('relu')(x)
#     x = MaxPool3D(pool_size=(2, 2, 2))(x) 

    x = residual_block(merged_layer, input_channels=n_channels, output_channels=16)
    x = attention_block(x, encoder_depth=1)
    
    x = Flatten()(x)
    

    ####################### INCEPTION ########################################
    base_network = inception_CNN_2_5d(input_shape,
                                    depth=depth,
                                    activation=activation,
                                    use_dropout=use_dropout,
                                    use_pooling=use_pooling_before_merge,
                                    kernel_regularizer = kernel_regularizer,
                                    activity_regularizer = activity_regularizer)



    # Re-use same instance of base network (weights are shared across both branches)
    processed_left = base_network(input_left)
    processed_right = base_network(input_right)
    
    # Maxpooling
    if use_pooling_before_merge:
        processed_left = MaxPooling3D(strides=(2, 2, 2), padding='same')(processed_left)
        processed_right = MaxPooling3D(strides=(2, 2, 2), padding='same')(processed_right)

    #Subtract
    merged_layer = Subtract()([processed_left, processed_right])
    
    filters = 2

#     kernel_regularizer = regularizers.l1_l2(l1=0.001, l2=0.001)
    kernel_regularizer = None
    
    # Inception module
    tower_1 = Conv3D(filters, kernel_size=(1, 1, 1), padding='same', activation=activation, kernel_regularizer=kernel_regularizer)(merged_layer)
    tower_2 = Conv3D(filters, kernel_size=(5, 5, 5), padding='same', activation=activation, kernel_regularizer=kernel_regularizer)(merged_layer)

    tower_3 = Conv3D(filters, kernel_size=(5, 5, 1), padding='same', activation=activation, kernel_regularizer=kernel_regularizer)(merged_layer)
    
    tower_4 = MaxPooling3D((3, 3, 3), strides=(1, 1, 1), padding='same')(merged_layer)
    tower_4 = Conv3D(filters, kernel_size=(1, 1, 1), padding='same', activation=activation, kernel_regularizer=kernel_regularizer)(tower_4)


    merged_layer = Concatenate(axis=4)([tower_1, tower_2, tower_4])

    
    if use_dropout:
        merged_layer = Dropout(0.05)(merged_layer)

    # Maxpooling
    if use_final_pooling:
        merged_layer = MaxPooling3D(strides=(2, 2, 2), padding='same')(merged_layer)

    # Flatten
    merged_layer = Flatten()(merged_layer)
    merged_layer = Concatenate()([merged_layer, x])
    
    
    #Dropout
#     merged_layer = Dropout(0.05)(merged_layer)
    
    # Prediction layer
    prediction = Dense(units=n_classes, activation='softmax')(merged_layer)


    return Model(inputs=[input_left, input_right], outputs=prediction)

def symmetric_ResidualAttention_Inception_subtract_addInitialInception(input_shape,
                                        depth=2,
                                        n_inception=1, # # inception modules after merge
                                        activation='relu',
                                        use_dropout=False,
                                        use_pooling_before_merge=False,
                                        use_pooling_after_merge=False,
                                        use_final_pooling=True,
                                        n_classes=2,
                                        kernel_regularizer = None,
                                        activity_regularizer = None,
                                        n_channels=8,
                                        nLayersForFeatExtr=1):
    
    #Input
    input_left = Input(shape=(input_shape))
    input_right = Input(shape=(input_shape))    
    
    
    ####################### ATTENTION ########################################
    attn_network = residualAttentionNetwork(shape=input_shape, 
                                              n_channels=n_channels, 
                                              n_classes=n_classes, 
                                              nLayersForFeatExtr=nLayersForFeatExtr)
    
    # Re-use same instance of base network (weights are shared across both branches)
    processed_left_attn = attn_network(input_left)
    processed_right_attn = attn_network(input_right)
    
    
    merged_layer = Subtract()([processed_left_attn, processed_right_attn])
    merged_layer = Concatenate()([merged_layer, processed_left_attn, processed_right_attn])

    # Modules after L-1 merge
    x = Conv3D(n_channels, (3, 3, 3), padding='same')(merged_layer)
    x = BatchNormalization()(x)
    x = Activation('relu')(x)
    x = MaxPool3D(pool_size=(2, 2, 2))(x) 

    x = residual_block(x, input_channels=n_channels, output_channels=64)
    
    x = Flatten()(x)
    

    ####################### INCEPTION ########################################
    base_network = inception_CNN_2_5d(input_shape,
                                    depth=depth,
                                    activation=activation,
                                    use_dropout=use_dropout,
                                    use_pooling=use_pooling_before_merge,
                                    kernel_regularizer = kernel_regularizer,
                                    activity_regularizer = activity_regularizer)



    # Re-use same instance of base network (weights are shared across both branches)
    processed_left = base_network(input_left)
    processed_right = base_network(input_right)
    
    # Maxpooling
    if use_pooling_before_merge:
        processed_left = MaxPooling3D(strides=(2, 2, 2), padding='same')(processed_left)
        processed_right = MaxPooling3D(strides=(2, 2, 2), padding='same')(processed_right)

    #Subtract
    merged_layer = Subtract()([processed_left, processed_right])
    
    #Concatenate previous images
#     merged_layer = Concatenate()([merged_layer, processed_left, processed_right])
    
    filters = 2
#     kernel_regularizer = regularizers.l1_l2(l1=0.001, l2=0.001)
    kernel_regularizer = None
    
    # Inception module
    tower_1 = Conv3D(filters, kernel_size=(1, 1, 1), padding='same', activation=activation, kernel_regularizer=kernel_regularizer)(merged_layer)
    tower_2 = Conv3D(filters, kernel_size=(5, 5, 5), padding='same', activation=activation, kernel_regularizer=kernel_regularizer)(merged_layer)

    tower_3 = Conv3D(filters, kernel_size=(5, 5, 1), padding='same', activation=activation, kernel_regularizer=kernel_regularizer)(merged_layer)
    
    tower_4 = MaxPooling3D((3, 3, 3), strides=(1, 1, 1), padding='same')(merged_layer)
    tower_4 = Conv3D(filters, kernel_size=(1, 1, 1), padding='same', activation=activation, kernel_regularizer=kernel_regularizer)(tower_4)


    merged_layer = Concatenate(axis=4)([tower_1, tower_2, tower_4])

    
    if use_dropout:
        merged_layer = Dropout(0.05)(merged_layer)

    # Maxpooling
    if use_final_pooling:
        merged_layer = MaxPooling3D(strides=(2, 2, 2), padding='same')(merged_layer)

    # Flatten
    merged_layer = Flatten()(merged_layer)
    merged_layer = Concatenate()([merged_layer, x])
    
    
    #Dropout
#     merged_layer = Dropout(0.05)(merged_layer)
    
    # Prediction layer
    prediction = Dense(units=n_classes, activation='softmax')(merged_layer)


    return Model(inputs=[input_left, input_right], outputs=prediction)

def symmetric_ResidualAttention_Inception_subtract_addInitialAttn_clinVarSeparateStream(input_shape,
                                        depth=2,
                                        n_inception=1, # # inception modules after merge
                                        activation='relu',
                                        use_dropout=False,
                                        use_pooling_before_merge=False,
                                        use_pooling_after_merge=False,
                                        use_final_pooling=True,
                                        n_classes=2,
                                        kernel_regularizer = None,
                                        activity_regularizer = None,
                                        n_channels=8,
                                        nLayersForFeatExtr=1):
    
    #Input
    input_left = Input(shape=(input_shape))
    input_right = Input(shape=(input_shape))  
    input_clinical_var = Input(shape=(30,))
    
    
    ####################### ATTENTION ########################################
    attn_network = residualAttentionNetwork(shape=input_shape, 
                                              n_channels=n_channels, 
                                              n_classes=n_classes, 
                                              nLayersForFeatExtr=nLayersForFeatExtr)
    
    # Re-use same instance of base network (weights are shared across both branches)
    processed_left_attn = attn_network(input_left)
    processed_right_attn = attn_network(input_right)

    merged_layer = Subtract()([processed_left_attn, processed_right_attn])
    merged_layer = Concatenate()([merged_layer, processed_left_attn, processed_right_attn])
#     merged_layer = Concatenate()([merged_layer, processed_left_attn]) #no need to have dedundant information

    # Modules after L-1 merge
    x = Conv3D(n_channels, (3, 3, 3), padding='same')(merged_layer)
    x = BatchNormalization()(x)
    x = Activation('relu')(x)
    x = MaxPool3D(pool_size=(2, 2, 2))(x) 

    x = residual_block(x, input_channels=n_channels, output_channels=64)
    
    x = Flatten()(x)

    

    ####################### INCEPTION ########################################
    base_network = inception_CNN_2_5d(input_shape,
                                    depth=depth,
                                    activation=activation,
                                    use_dropout=use_dropout,
                                    use_pooling=use_pooling_before_merge,
                                    kernel_regularizer = kernel_regularizer,
                                    activity_regularizer = activity_regularizer)



    # Re-use same instance of base network (weights are shared across both branches)
    processed_left = base_network(input_left)
    processed_right = base_network(input_right)
    
    # Maxpooling
    if use_pooling_before_merge:
        processed_left = MaxPooling3D(strides=(2, 2, 2), padding='same')(processed_left)
        processed_right = MaxPooling3D(strides=(2, 2, 2), padding='same')(processed_right)
        
    #Subtract
    merged_layer = Subtract()([processed_left, processed_right])
    
    
    filters = 2
#     kernel_regularizer = regularizers.l1_l2(l1=0.001, l2=0.001)
    kernel_regularizer = None
    
    # Inception module
    tower_1 = Conv3D(filters, kernel_size=(1, 1, 1), padding='same', activation=activation, kernel_regularizer=kernel_regularizer)(merged_layer)
    tower_2 = Conv3D(filters, kernel_size=(5, 5, 5), padding='same', activation=activation, kernel_regularizer=kernel_regularizer)(merged_layer)

    tower_3 = Conv3D(filters, kernel_size=(5, 5, 1), padding='same', activation=activation, kernel_regularizer=kernel_regularizer)(merged_layer)
    
    tower_4 = MaxPooling3D((3, 3, 3), strides=(1, 1, 1), padding='same')(merged_layer)
    tower_4 = Conv3D(filters, kernel_size=(1, 1, 1), padding='same', activation=activation, kernel_regularizer=kernel_regularizer)(tower_4)


    merged_layer = Concatenate(axis=4)([tower_1, tower_2, tower_4])

    
    if use_dropout:
        merged_layer = Dropout(0.05)(merged_layer)

    # Maxpooling
    if use_final_pooling:
        
        merged_layer = MaxPooling3D(strides=(4, 4, 4), padding='same')(merged_layer)
        
    # Flatten
    merged_layer = Flatten()(merged_layer)
    merged_layer = Concatenate()([merged_layer, x])
    
    # Prediction layer imaging
    prediction_imaging = Dense(units=n_classes, activation='softmax', name='imaging')(merged_layer)
    
    # Prediction layer clinical variables
    prediction_clinical = Dense(units=n_classes, activation='softmax', name='clinical_var')(input_clinical_var)

    return Model(inputs=[input_left, input_right, input_clinical_var], outputs=[prediction_imaging, prediction_clinical])

def symmetric_ResidualAttention_Inception_subtract_addInitialAttn_clinVarSeparateStream_addedPrediction(input_shape,
                                        depth=2,
                                        n_inception=1, # # inception modules after merge
                                        activation='relu',
                                        use_dropout=False,
                                        use_pooling_before_merge=False,
                                        use_pooling_after_merge=False,
                                        use_final_pooling=True,
                                        n_classes=2,
                                        kernel_regularizer = None,
                                        activity_regularizer = None,
                                        n_channels=8,
                                        nLayersForFeatExtr=1):
    
    #Input
    input_left = Input(shape=(input_shape))
    input_right = Input(shape=(input_shape))  
    input_clinical_var = Input(shape=(30,)) #with longitudinal variables
#     input_clinical_var = Input(shape=(12,)) #no APOE
#     input_clinical_var = Input(shape=(21,))
    
    ####################### ATTENTION ########################################
    attn_network = residualAttentionNetwork(shape=input_shape, 
                                              n_channels=n_channels, 
                                              n_classes=n_classes, 
                                              nLayersForFeatExtr=nLayersForFeatExtr)
    
    # Re-use same instance of base network (weights are shared across both branches)
    processed_left_attn = attn_network(input_left)
    processed_right_attn = attn_network(input_right)

    merged_layer = Subtract()([processed_left_attn, processed_right_attn])
#     merged_layer = Concatenate()([merged_layer, processed_left_attn, processed_right_attn])
    merged_layer = Concatenate()([merged_layer, processed_left_attn]) #no need to have dedundant information

    # Modules after L-1 merge
    x = Conv3D(n_channels, (3, 3, 3), padding='same')(merged_layer)
    x = BatchNormalization()(x)
    x = Activation('relu')(x)
    x = MaxPool3D(pool_size=(2, 2, 2))(x) 

    x = residual_block(x, input_channels=n_channels, output_channels=64)
    
    x = Flatten()(x)

    

    ####################### INCEPTION ########################################
    base_network = inception_CNN_2_5d(input_shape,
                                    depth=depth,
                                    activation=activation,
                                    use_dropout=use_dropout,
                                    use_pooling=use_pooling_before_merge,
                                    kernel_regularizer = kernel_regularizer,
                                    activity_regularizer = activity_regularizer)



    # Re-use same instance of base network (weights are shared across both branches)
    processed_left = base_network(input_left)
    processed_right = base_network(input_right)
    
    # Maxpooling
    if use_pooling_before_merge:
        processed_left = MaxPooling3D(strides=(2, 2, 2), padding='same')(processed_left)
        processed_right = MaxPooling3D(strides=(2, 2, 2), padding='same')(processed_right)
        
    #Subtract
    merged_layer = Subtract()([processed_left, processed_right])
    
    
    filters = 2
#     kernel_regularizer = regularizers.l1_l2(l1=0.001, l2=0.001)
    kernel_regularizer = None
    
#     # Inception module
#     tower_1 = Conv3D(filters, kernel_size=(1, 1, 1), padding='same', activation=activation, kernel_regularizer=kernel_regularizer)(merged_layer)
#     tower_2 = Conv3D(filters, kernel_size=(5, 5, 5), padding='same', activation=activation, kernel_regularizer=kernel_regularizer)(merged_layer)

#     tower_3 = Conv3D(filters, kernel_size=(5, 5, 1), padding='same', activation=activation, kernel_regularizer=kernel_regularizer)(merged_layer)
    
#     tower_4 = MaxPooling3D((3, 3, 3), strides=(1, 1, 1), padding='same')(merged_layer)
#     tower_4 = Conv3D(filters, kernel_size=(1, 1, 1), padding='same', activation=activation, kernel_regularizer=kernel_regularizer)(tower_4)

    ########## ALTERNATE INCEPTION ##########
     # Inception module
    tower_1 = Conv3D(filters, kernel_size=(5, 1, 1), padding='same', activation=activation, kernel_regularizer=kernel_regularizer)(merged_layer)
    tower_2 = Conv3D(filters, kernel_size=(1, 5, 1), padding='same', activation=activation, kernel_regularizer=kernel_regularizer)(merged_layer)
    
    tower_4 = MaxPooling3D((3, 3, 3), strides=(1, 1, 1), padding='same')(merged_layer)
    tower_4 = Conv3D(filters, kernel_size=(1, 1, 5), padding='same', activation=activation, kernel_regularizer=kernel_regularizer)(tower_4)


    merged_layer = Concatenate(axis=4)([tower_1, tower_2, tower_4])

    
    if use_dropout:
        merged_layer = Dropout(0.05)(merged_layer)

    # Maxpooling
    if use_final_pooling:
        
        merged_layer = MaxPooling3D(strides=(4, 4, 4), padding='same')(merged_layer)
        
    # Flatten
    merged_layer = Flatten()(merged_layer)
    merged_layer = Concatenate()([merged_layer, x])
    
    # Prediction layer imaging
    prediction_imaging = Dense(units=1, activation='sigmoid', name='imaging')(merged_layer)
    
    # Prediction layer clinical variables
    prediction_clinical = Dense(units=1, activation='sigmoid', name='clinical_var')(input_clinical_var)
    
    #Concatenate
    merged_predictions = Concatenate()([prediction_imaging, prediction_clinical])
    
    # Final Prediction layer
    prediction = Dense(units=n_classes, activation='softmax', name='final_prediction')(merged_predictions)

    return Model(inputs=[input_left, input_right, input_clinical_var], outputs=prediction)

def symmetric_ResidualAttention_Inception_subtract_addInitialAttn_clinVarSeparateStream_addedPrediction2(input_shape,
                                        depth=2,
                                        n_inception=1, # # inception modules after merge
                                        activation='relu',
                                        use_dropout=False,
                                        use_pooling_before_merge=False,
                                        use_pooling_after_merge=False,
                                        use_final_pooling=True,
                                        n_classes=2,
                                        kernel_regularizer = None,
                                        activity_regularizer = None,
                                        n_channels=8,
                                        nLayersForFeatExtr=1):
    
    #Input
    input_left = Input(shape=(input_shape))
    input_right = Input(shape=(input_shape))  
    input_clinical_var = Input(shape=(30,)) #with longitudinal variables
#     input_clinical_var = Input(shape=(12,)) #no APOE
#     input_clinical_var = Input(shape=(21,))
    
    
    ####################### ATTENTION ########################################
    attn_network = residualAttentionNetwork(shape=input_shape, 
                                              n_channels=n_channels, 
                                              n_classes=n_classes, 
                                              nLayersForFeatExtr=nLayersForFeatExtr)
    
    # Re-use same instance of base network (weights are shared across both branches)
    processed_left_attn = attn_network(input_left)
    processed_right_attn = attn_network(input_right)

    merged_layer = Subtract()([processed_left_attn, processed_right_attn])
    merged_layer = Concatenate()([merged_layer, processed_left_attn, processed_right_attn])

    # Modules after L-1 merge
    x = Conv3D(n_channels, (3, 3, 3), padding='same')(merged_layer)
    x = BatchNormalization()(x)
    x = Activation('relu')(x)
    x = MaxPool3D(pool_size=(4,4,4))(x) 

    x = residual_block(x, input_channels=n_channels, output_channels=64)

    x = Flatten()(x)

    ####################### INCEPTION ########################################
    base_network = inception_CNN_2_5d(input_shape,
                                    depth=depth,
                                    activation=activation,
                                    use_dropout=use_dropout,
                                    use_pooling=use_pooling_before_merge,
                                    kernel_regularizer = kernel_regularizer,
                                    activity_regularizer = activity_regularizer)



    # Re-use same instance of base network (weights are shared across both branches)
    processed_left = base_network(input_left)
    processed_right = base_network(input_right)
    
    # Maxpooling
    if use_pooling_before_merge:
        processed_left = MaxPooling3D(strides=(2, 2, 2), padding='same')(processed_left)
        processed_right = MaxPooling3D(strides=(2, 2, 2), padding='same')(processed_right)
        
    #Subtract
    merged_layer = Subtract()([processed_left, processed_right])
    

    filters = 2
#     kernel_regularizer = None
    kernel_regularizer = regularizers.l1_l2(l1=0.001, l2=0.001)

    ########## ALTERNATE INCEPTION ##########
     # Inception module
    tower_1 = Conv3D(filters, kernel_size=(5, 1, 1), padding='same', activation=activation, kernel_regularizer=kernel_regularizer)(merged_layer)
    tower_2 = Conv3D(filters, kernel_size=(1, 5, 1), padding='same', activation=activation, kernel_regularizer=kernel_regularizer)(merged_layer)
    
    tower_4 = MaxPooling3D((3, 3, 3), strides=(1, 1, 1), padding='same')(merged_layer)
    tower_4 = Conv3D(filters, kernel_size=(1, 1, 5), padding='same', activation=activation, kernel_regularizer=kernel_regularizer)(tower_4)

    #1D 5,1,1 1,5,1 1,1,5
    #2D 1,5,5 5,5,1 5,1,5

    merged_layer = Concatenate(axis=4)([tower_1, tower_2, tower_4])

    # Maxpooling
    if use_final_pooling:
        
#         merged_layer = MaxPooling3D(strides=(4, 4, 4), padding='same')(merged_layer)
        merged_layer = MaxPooling3D(strides=(8,8,8), padding='same')(merged_layer) #test166
        merged_layer = AveragePooling3D(strides=(2,2,2), padding='same')(merged_layer) #test168
        

    # Flatten
    merged_layer = Flatten()(merged_layer)
    merged_layer = Concatenate()([merged_layer, x])
    merged_layer = Dense(16)(merged_layer) 
    
    # Prediction layer imaging
    prediction_imaging = Dense(units=1, activation='sigmoid', name='imaging')(merged_layer)
    
    # Prediction layer clinical variables
    prediction_clinical = Dense(units=1, activation='sigmoid', name='clinical_var')(input_clinical_var)
    
    #Concatenate
    merged_predictions = Concatenate()([prediction_imaging, prediction_clinical])
    
    # Final Prediction layer
    prediction = Dense(units=n_classes, activation='softmax', name='final_prediction')(merged_predictions)

    return Model(inputs=[input_left, input_right, input_clinical_var], outputs=prediction)

def symmetric_ResidualAttention_Inception_subtract_addInitialAttn_clinVarSeparateStream_addedPrediction_Ventricles(input_shape,
                                        depth=2,
                                        n_inception=1, # # inception modules after merge
                                        activation='relu',
                                        use_dropout=False,
                                        use_pooling_before_merge=False,
                                        use_pooling_after_merge=False,
                                        use_final_pooling=True,
                                        n_classes=2,
                                        kernel_regularizer = None,
                                        activity_regularizer = None,
                                        n_channels=8,
                                        nLayersForFeatExtr=1):
    
    #Input
    input_left = Input(shape=(input_shape))
    input_right = Input(shape=(input_shape))  
    input_clinical_var = Input(shape=(30,)) #with longitudinal variables
    input_ventricle = Input(shape=(1,)) #with longitudinal variables
#     input_clinical_var = Input(shape=(12,)) #no APOE
#     input_clinical_var = Input(shape=(21,))
    
    
    ####################### ATTENTION ########################################
    attn_network = residualAttentionNetwork(shape=input_shape, 
                                              n_channels=n_channels, 
                                              n_classes=n_classes, 
                                              nLayersForFeatExtr=nLayersForFeatExtr)
    
    # Re-use same instance of base network (weights are shared across both branches)
    processed_left_attn = attn_network(input_left)
    processed_right_attn = attn_network(input_right)

    merged_layer = Subtract()([processed_left_attn, processed_right_attn])
    merged_layer = Concatenate()([merged_layer, processed_left_attn, processed_right_attn])

    # Modules after L-1 merge
    x = Conv3D(n_channels, (3, 3, 3), padding='same')(merged_layer)
    x = BatchNormalization()(x)
    x = Activation('relu')(x)
    x = MaxPool3D(pool_size=(4,4,4))(x) 

    x = residual_block(x, input_channels=n_channels, output_channels=64)

    x = Flatten()(x)

    ####################### INCEPTION ########################################
    base_network = inception_CNN_2_5d(input_shape,
                                    depth=depth,
                                    activation=activation,
                                    use_dropout=use_dropout,
                                    use_pooling=use_pooling_before_merge,
                                    kernel_regularizer = kernel_regularizer,
                                    activity_regularizer = activity_regularizer)



    # Re-use same instance of base network (weights are shared across both branches)
    processed_left = base_network(input_left)
    processed_right = base_network(input_right)
    
    # Maxpooling
    if use_pooling_before_merge:
        processed_left = MaxPooling3D(strides=(2, 2, 2), padding='same')(processed_left)
        processed_right = MaxPooling3D(strides=(2, 2, 2), padding='same')(processed_right)
        
    #Subtract
    merged_layer = Subtract()([processed_left, processed_right])
    

    filters = 2
#     kernel_regularizer = None
    kernel_regularizer = regularizers.l1_l2(l1=0.001, l2=0.001)

    ########## ALTERNATE INCEPTION ##########
     # Inception module
    tower_1 = Conv3D(filters, kernel_size=(5, 1, 1), padding='same', activation=activation, kernel_regularizer=kernel_regularizer)(merged_layer)
    tower_2 = Conv3D(filters, kernel_size=(1, 5, 1), padding='same', activation=activation, kernel_regularizer=kernel_regularizer)(merged_layer)
    
    tower_4 = MaxPooling3D((3, 3, 3), strides=(1, 1, 1), padding='same')(merged_layer)
    tower_4 = Conv3D(filters, kernel_size=(1, 1, 5), padding='same', activation=activation, kernel_regularizer=kernel_regularizer)(tower_4)

    #1D 5,1,1 1,5,1 1,1,5
    #2D 1,5,5 5,5,1 5,1,5

    merged_layer = Concatenate(axis=4)([tower_1, tower_2, tower_4])

    # Maxpooling
    if use_final_pooling:
        
#         merged_layer = MaxPooling3D(strides=(4, 4, 4), padding='same')(merged_layer)
        merged_layer = MaxPooling3D(strides=(8,8,8), padding='same')(merged_layer) #test166
#         merged_layer = AveragePooling3D(strides=(2,2,2), padding='same')(merged_layer) #test168
        

    # Flatten
    merged_layer = Flatten()(merged_layer)
    merged_layer = Concatenate()([merged_layer, x])
    merged_layer = Dense(16)(merged_layer) 
    
    # Prediction layer imaging
    prediction_imaging = Dense(units=1, activation='sigmoid', name='imaging')(merged_layer)
    
    # Prediction layer clinical variables
    prediction_clinical = Dense(units=1, activation='sigmoid', name='clinical_var')(input_clinical_var)
    
    #Concatenate
    merged_predictions = Concatenate()([prediction_imaging, prediction_clinical, input_ventricle])
    
    # Final Prediction layer
    prediction = Dense(units=n_classes, activation='softmax', name='final_prediction')(merged_predictions)

    return Model(inputs=[input_left, input_right, input_clinical_var, input_ventricle], outputs=prediction)


def symmetric_ResidualAttention_Inception_subtract_addInitialAttn_clinVarSeparateStream_addedPrediction_semiTrainable(input_shape,
                                        depth=2,
                                        n_inception=1, # # inception modules after merge
                                        activation='relu',
                                        use_dropout=False,
                                        use_pooling_before_merge=False,
                                        use_pooling_after_merge=False,
                                        use_final_pooling=True,
                                        n_classes=2,
                                        kernel_regularizer = None,
                                        activity_regularizer = None,
                                        n_channels=8,
                                        nLayersForFeatExtr=1):
    
    #Input
    input_left = Input(shape=(input_shape))
    input_right = Input(shape=(input_shape))  
    input_clinical_var = Input(shape=(30,))
    
    #residualAttentionNetwork
    #residualAttentionNetwork_semiTrainable
    ####################### ATTENTION ########################################
    attn_network = residualAttentionNetwork(shape=input_shape, 
                                              n_channels=n_channels, 
                                              n_classes=n_classes, 
                                              nLayersForFeatExtr=nLayersForFeatExtr)
    

    # Re-use same instance of base network (weights are shared across both branches)
    processed_left_attn = attn_network(input_left)
    processed_right_attn = attn_network(input_right)

    merged_layer = Subtract()([processed_left_attn, processed_right_attn])
    merged_layer = Concatenate()([merged_layer, processed_left_attn, processed_right_attn])

    # Modules after L-1 merge
    x = Conv3D(n_channels, (3, 3, 3), padding='same')(merged_layer)
    x = BatchNormalization()(x)
    x = Activation('relu')(x)
    x = MaxPool3D(pool_size=(2, 2, 2))(x) 

    x = residual_block(x, input_channels=n_channels, output_channels=64)
    
    x = Flatten()(x)

    
    #inception_CNN_2_5d
    #inception_CNN_2_5d_semiTrainable
    ####################### INCEPTION ########################################
    base_network = inception_CNN_2_5d_semiTrainable(input_shape,
                                    depth=depth,
                                    activation=activation,
                                    use_dropout=use_dropout,
                                    use_pooling=use_pooling_before_merge,
                                    kernel_regularizer = kernel_regularizer,
                                    activity_regularizer = activity_regularizer)



    # Re-use same instance of base network (weights are shared across both branches)
    processed_left = base_network(input_left)
    processed_right = base_network(input_right)
    
    # Maxpooling
    if use_pooling_before_merge:
        processed_left = MaxPooling3D(strides=(2, 2, 2), padding='same')(processed_left)
        processed_right = MaxPooling3D(strides=(2, 2, 2), padding='same')(processed_right)
        
    #Subtract
    merged_layer = Subtract()([processed_left, processed_right])
    
    
    filters = 2
#     kernel_regularizer = regularizers.l1_l2(l1=0.001, l2=0.001)
    kernel_regularizer = None
    
    # Inception module
    tower_1 = Conv3D(filters, kernel_size=(1, 1, 1), padding='same', activation=activation, kernel_regularizer=kernel_regularizer)(merged_layer)
    tower_2 = Conv3D(filters, kernel_size=(5, 5, 5), padding='same', activation=activation, kernel_regularizer=kernel_regularizer)(merged_layer)

    tower_3 = Conv3D(filters, kernel_size=(5, 5, 1), padding='same', activation=activation, kernel_regularizer=kernel_regularizer)(merged_layer)
    
    tower_4 = MaxPooling3D((3, 3, 3), strides=(1, 1, 1), padding='same')(merged_layer)
    tower_4 = Conv3D(filters, kernel_size=(1, 1, 1), padding='same', activation=activation, kernel_regularizer=kernel_regularizer)(tower_4)


    merged_layer = Concatenate(axis=4)([tower_1, tower_2, tower_4])

    
    if use_dropout:
        merged_layer = Dropout(0.05)(merged_layer)

    # Maxpooling
    if use_final_pooling:
        
        merged_layer = MaxPooling3D(strides=(4, 4, 4), padding='same')(merged_layer)
        
    # Flatten
    merged_layer = Flatten()(merged_layer)
    merged_layer = Concatenate()([merged_layer, x])
    
    # Prediction layer imaging
    prediction_imaging = Dense(units=1, activation='sigmoid', name='imaging')(merged_layer)
    
    # Prediction layer clinical variables
    prediction_clinical = Dense(units=1, activation='sigmoid', name='clinical_var')(input_clinical_var)
    
    #Concatenate
    merged_predictions = Concatenate()([prediction_imaging, prediction_clinical])
    
    # Final Prediction layer
    prediction = Dense(units=n_classes, activation='softmax', name='final_prediction')(merged_predictions)

    return Model(inputs=[input_left, input_right, input_clinical_var], outputs=prediction)

def symmetric_ResidualAttention_Inception_subtract_addInitialAttn_clinVarSeparateStream_addedPrediction_onlyLastLayer(input_shape,
                                        depth=2,
                                        n_inception=1, # # inception modules after merge
                                        activation='relu',
                                        use_dropout=False,
                                        use_pooling_before_merge=False,
                                        use_pooling_after_merge=False,
                                        use_final_pooling=True,
                                        n_classes=2,
                                        kernel_regularizer = None,
                                        activity_regularizer = None,
                                        n_channels=8,
                                        nLayersForFeatExtr=1):
    
    #Input
    input_left = Input(shape=(input_shape))
    input_right = Input(shape=(input_shape))  
    input_clinical_var = Input(shape=(30,))
    
    
    ####################### ATTENTION ########################################
    attn_network = residualAttentionNetwork_semiTrainable(shape=input_shape, 
                                              n_channels=n_channels, 
                                              n_classes=n_classes, 
                                              nLayersForFeatExtr=nLayersForFeatExtr)
    
    # Re-use same instance of base network (weights are shared across both branches)
    processed_left_attn = attn_network(input_left)
    processed_right_attn = attn_network(input_right)

    merged_layer = Subtract()([processed_left_attn, processed_right_attn])
    merged_layer = Concatenate()([merged_layer, processed_left_attn, processed_right_attn])

    # Modules after L-1 merge
    x = Conv3D(n_channels, (3, 3, 3), padding='same', trainable=False)(merged_layer)
    x = BatchNormalization()(x)
    x = Activation('relu')(x)
    x = MaxPool3D(pool_size=(2, 2, 2))(x) 

    x = residual_block_semiTrainable(x, input_channels=n_channels, output_channels=64)
    
    x = Flatten()(x)

    

    ####################### INCEPTION ########################################
    base_network = inception_CNN_2_5d_semiTrainable(input_shape,
                                    depth=depth,
                                    activation=activation,
                                    use_dropout=use_dropout,
                                    use_pooling=use_pooling_before_merge,
                                    kernel_regularizer = kernel_regularizer,
                                    activity_regularizer = activity_regularizer)



    # Re-use same instance of base network (weights are shared across both branches)
    processed_left = base_network(input_left)
    processed_right = base_network(input_right)
    
    # Maxpooling
    if use_pooling_before_merge:
        processed_left = MaxPooling3D(strides=(2, 2, 2), padding='same')(processed_left)
        processed_right = MaxPooling3D(strides=(2, 2, 2), padding='same')(processed_right)
        
    #Subtract
    merged_layer = Subtract()([processed_left, processed_right])
    
    
    filters = 2
#     kernel_regularizer = regularizers.l1_l2(l1=0.001, l2=0.001)
    kernel_regularizer = None
    
    # Inception module
    tower_1 = Conv3D(filters, kernel_size=(1, 1, 1), padding='same', activation=activation, kernel_regularizer=kernel_regularizer, trainable=False)(merged_layer)
    tower_2 = Conv3D(filters, kernel_size=(5, 5, 5), padding='same', activation=activation, kernel_regularizer=kernel_regularizer, trainable=False)(merged_layer)

    tower_3 = Conv3D(filters, kernel_size=(5, 5, 1), padding='same', activation=activation, kernel_regularizer=kernel_regularizer, trainable=False)(merged_layer)
    
    tower_4 = MaxPooling3D((3, 3, 3), strides=(1, 1, 1), padding='same')(merged_layer)
    tower_4 = Conv3D(filters, kernel_size=(1, 1, 1), padding='same', activation=activation, kernel_regularizer=kernel_regularizer, trainable=False)(tower_4)

    merged_layer = Concatenate(axis=4)([tower_1, tower_2, tower_4])

    
    if use_dropout:
        merged_layer = Dropout(0.05)(merged_layer)

    # Maxpooling
    if use_final_pooling:
        
        merged_layer = MaxPooling3D(strides=(4, 4, 4), padding='same')(merged_layer)
        
    # Flatten
    merged_layer = Flatten()(merged_layer)
    merged_layer = Concatenate()([merged_layer, x])
    
    # Prediction layer imaging
    prediction_imaging = Dense(units=1, activation='sigmoid', name='imaging')(merged_layer)
    
    # Prediction layer clinical variables
    prediction_clinical = Dense(units=1, activation='sigmoid', name='clinical_var')(input_clinical_var)
    
    #Concatenate
    merged_predictions = Concatenate()([prediction_imaging, prediction_clinical])
    
    # Final Prediction layer
    prediction = Dense(units=n_classes, activation='softmax', name='final_prediction')(merged_predictions)

    return Model(inputs=[input_left, input_right, input_clinical_var], outputs=prediction)


def symmetric_ResidualAttention_Inception_subtract_addInitialAttn_clinVarSeparateStream_addedLoss(input_shape,
                                        depth=2,
                                        n_inception=1, # # inception modules after merge
                                        activation='relu',
                                        use_dropout=False,
                                        use_pooling_before_merge=False,
                                        use_pooling_after_merge=False,
                                        use_final_pooling=True,
                                        n_classes=2,
                                        kernel_regularizer = None,
                                        activity_regularizer = None,
                                        n_channels=8,
                                        nLayersForFeatExtr=1):
    
    #Input
    input_left = Input(shape=(input_shape))
    input_right = Input(shape=(input_shape))  
    input_clinical_var = Input(shape=(30,))
    target = Input(shape=(1,))
    
    
    ####################### ATTENTION ########################################
    attn_network = residualAttentionNetwork(shape=input_shape, 
                                              n_channels=n_channels, 
                                              n_classes=n_classes, 
                                              nLayersForFeatExtr=nLayersForFeatExtr)
    
    # Re-use same instance of base network (weights are shared across both branches)
    processed_left_attn = attn_network(input_left)
    processed_right_attn = attn_network(input_right)

    merged_layer = Subtract()([processed_left_attn, processed_right_attn])
    merged_layer = Concatenate()([merged_layer, processed_left_attn, processed_right_attn])

    # Modules after L-1 merge
    x = Conv3D(n_channels, (3, 3, 3), padding='same')(merged_layer)
    x = BatchNormalization()(x)
    x = Activation('relu')(x)
    x = MaxPool3D(pool_size=(2, 2, 2))(x) 

    x = residual_block(x, input_channels=n_channels, output_channels=64)
    
    x = Flatten()(x)

    

    ####################### INCEPTION ########################################
    base_network = inception_CNN_2_5d(input_shape,
                                    depth=depth,
                                    activation=activation,
                                    use_dropout=use_dropout,
                                    use_pooling=use_pooling_before_merge,
                                    kernel_regularizer = kernel_regularizer,
                                    activity_regularizer = activity_regularizer)



    # Re-use same instance of base network (weights are shared across both branches)
    processed_left = base_network(input_left)
    processed_right = base_network(input_right)
    
    # Maxpooling
    if use_pooling_before_merge:
        processed_left = MaxPooling3D(strides=(2, 2, 2), padding='same')(processed_left)
        processed_right = MaxPooling3D(strides=(2, 2, 2), padding='same')(processed_right)
        
    #Subtract
    merged_layer = Subtract()([processed_left, processed_right])
    
    
    filters = 2
#     kernel_regularizer = regularizers.l1_l2(l1=0.001, l2=0.001)
    kernel_regularizer = None
    
    # Inception module
    tower_1 = Conv3D(filters, kernel_size=(1, 1, 1), padding='same', activation=activation, kernel_regularizer=kernel_regularizer)(merged_layer)
    tower_2 = Conv3D(filters, kernel_size=(5, 5, 5), padding='same', activation=activation, kernel_regularizer=kernel_regularizer)(merged_layer)

    tower_3 = Conv3D(filters, kernel_size=(5, 5, 1), padding='same', activation=activation, kernel_regularizer=kernel_regularizer)(merged_layer)
    
    tower_4 = MaxPooling3D((3, 3, 3), strides=(1, 1, 1), padding='same')(merged_layer)
    tower_4 = Conv3D(filters, kernel_size=(1, 1, 1), padding='same', activation=activation, kernel_regularizer=kernel_regularizer)(tower_4)


    merged_layer = Concatenate(axis=4)([tower_1, tower_2, tower_4])

    
    if use_dropout:
        merged_layer = Dropout(0.05)(merged_layer)

    # Maxpooling
    if use_final_pooling:
        
        merged_layer = MaxPooling3D(strides=(4, 4, 4), padding='same')(merged_layer)
        
    # Flatten
    merged_layer = Flatten()(merged_layer)
    merged_layer = Concatenate()([merged_layer, x])
    
    # Prediction layer imaging
    prediction_imaging = Dense(units=1, activation='sigmoid', name='imaging')(merged_layer)
    
    # Prediction layer clinical variables
    prediction_clinical = Dense(units=1, activation='sigmoid', name='clinical_var')(input_clinical_var)
    
    #Calculate loss
    calculate_loss_imaging = Lambda(lambda tensors: binary_crossentropy(target, tensors, from_logits=False), name='calculate_loss_imaging')
    prediction_imaging_loss = calculate_loss_imaging(prediction_imaging)
    
    calculate_loss_clinical = Lambda(lambda tensors: binary_crossentropy(target, tensors, from_logits=False), name='calculate_loss_clinical')
    prediction_clinical_loss = calculate_loss_clinical(prediction_clinical)
    
    #Concatenate
    merged_losses = Concatenate()([prediction_imaging_loss, prediction_clinical_loss])
    
    # Final Prediction layer
    prediction = Dense(units=n_classes, activation='softmax', name='final_prediction')(merged_losses)

    return Model(inputs=[input_left, input_right, input_clinical_var, target], outputs=prediction)

#Used in binary crossentropy below
def sigmoid(x):
    return 1. / (1. + K.exp(-x))

#Copied from keras documentation
#Used to calculate loss within the model
def binary_crossentropy(target, output, from_logits=False):
    if not from_logits:
        output = K.clip(output, 1e-7, 1 - 1e-7)
        output = K.log(output / (1 - output))
    return (target * -K.log(sigmoid(output)) +
            (1 - target) * -K.log(1 - sigmoid(output)))

def symmetric_ResidualAttention_Inception_subtract_addInitialAttn_clinVar(input_shape,
                                        depth=2,
                                        n_inception=1, # # inception modules after merge
                                        activation='relu',
                                        use_dropout=False,
                                        use_pooling_before_merge=False,
                                        use_pooling_after_merge=False,
                                        use_final_pooling=True,
                                        n_classes=2,
                                        kernel_regularizer = None,
                                        activity_regularizer = None,
                                        n_channels=8,
                                        nLayersForFeatExtr=1):
    
    #Input
    input_left = Input(shape=(input_shape))
    input_right = Input(shape=(input_shape))  
    input_clinical_var = Input(shape=(30,))
    
    
    ####################### ATTENTION ########################################
    attn_network = residualAttentionNetwork(shape=input_shape, 
                                              n_channels=n_channels, 
                                              n_classes=n_classes, 
                                              nLayersForFeatExtr=nLayersForFeatExtr)
    
    # Re-use same instance of base network (weights are shared across both branches)
    processed_left_attn = attn_network(input_left)
    processed_right_attn = attn_network(input_right)

    merged_layer = Subtract()([processed_left_attn, processed_right_attn])
    merged_layer = Concatenate()([merged_layer, processed_left_attn, processed_right_attn])

    # Modules after L-1 merge
    x = Conv3D(n_channels, (3, 3, 3), padding='same')(merged_layer)
    x = BatchNormalization()(x)
    x = Activation('relu')(x)
    x = MaxPool3D(pool_size=(2, 2, 2))(x) 

    x = residual_block(x, input_channels=n_channels, output_channels=64)
    
    x = Flatten()(x)

    

    ####################### INCEPTION ########################################
    base_network = inception_CNN_2_5d(input_shape,
                                    depth=depth,
                                    activation=activation,
                                    use_dropout=use_dropout,
                                    use_pooling=use_pooling_before_merge,
                                    kernel_regularizer = kernel_regularizer,
                                    activity_regularizer = activity_regularizer)



    # Re-use same instance of base network (weights are shared across both branches)
    processed_left = base_network(input_left)
    processed_right = base_network(input_right)
    
    # Maxpooling
    if use_pooling_before_merge:
        processed_left = MaxPooling3D(strides=(2, 2, 2), padding='same')(processed_left)
        processed_right = MaxPooling3D(strides=(2, 2, 2), padding='same')(processed_right)
        
    #Subtract
    merged_layer = Subtract()([processed_left, processed_right])
    
    
    filters = 2
#     kernel_regularizer = regularizers.l1_l2(l1=0.001, l2=0.001)
    kernel_regularizer = None
    
    # Inception module
    tower_1 = Conv3D(filters, kernel_size=(1, 1, 1), padding='same', activation=activation, kernel_regularizer=kernel_regularizer)(merged_layer)
    tower_2 = Conv3D(filters, kernel_size=(5, 5, 5), padding='same', activation=activation, kernel_regularizer=kernel_regularizer)(merged_layer)

    tower_3 = Conv3D(filters, kernel_size=(5, 5, 1), padding='same', activation=activation, kernel_regularizer=kernel_regularizer)(merged_layer)
    
    tower_4 = MaxPooling3D((3, 3, 3), strides=(1, 1, 1), padding='same')(merged_layer)
    tower_4 = Conv3D(filters, kernel_size=(1, 1, 1), padding='same', activation=activation, kernel_regularizer=kernel_regularizer)(tower_4)


    merged_layer = Concatenate(axis=4)([tower_1, tower_2, tower_4])

    
    if use_dropout:
        merged_layer = Dropout(0.05)(merged_layer)

    # Maxpooling
    if use_final_pooling:
        
        merged_layer = MaxPooling3D(strides=(4, 4, 4), padding='same')(merged_layer)
        
    # Flatten
    merged_layer = Flatten()(merged_layer)
    merged_layer = Concatenate()([merged_layer, x, input_clinical_var])
    
    # Prediction layer
    prediction = Dense(units=n_classes, activation='softmax')(merged_layer)

    return Model(inputs=[input_left, input_right, input_clinical_var], outputs=prediction)

def clinVarOnly(input_shape,
                depth=2,
                n_inception=1, # # inception modules after merge
                activation='relu',
                use_dropout=False,
                use_pooling_before_merge=False,
                use_pooling_after_merge=False,
                use_final_pooling=True,
                n_classes=2,
                kernel_regularizer = None,
                activity_regularizer = None,
                n_channels=8,
                nLayersForFeatExtr=1):
    
    #Input
    input_left = Input(shape=(input_shape))
    input_right = Input(shape=(input_shape))  
    input_clinical_var = Input(shape=(30,))
    
    # Prediction layer
    prediction = Dense(units=n_classes, activation='softmax')(input_clinical_var)

    return Model(inputs=[input_left, input_right, input_clinical_var], outputs=prediction)

def clinVar_imagingEmbedding(input_shape,
                depth=2,
                n_inception=1, # # inception modules after merge
                activation='relu',
                use_dropout=False,
                use_pooling_before_merge=False,
                use_pooling_after_merge=False,
                use_final_pooling=True,
                n_classes=2,
                kernel_regularizer = None,
                activity_regularizer = None,
                n_channels=8,
                nLayersForFeatExtr=1):
    
    #Input
    input_left = Input(shape=(input_shape))
    input_right = Input(shape=(input_shape))  
    input_clinical_var = Input(shape=(30,))
    input_imaging_embedding = Input(shape=(71680,))
    
    #Concatenate
    merged_layer = Concatenate()([input_imaging_embedding, input_clinical_var])
    
    # Prediction layer
    prediction = Dense(units=n_classes, activation='softmax')(merged_layer)

    return Model(inputs=[input_left, input_right, input_clinical_var, input_imaging_embedding], outputs=prediction)

def symmetric_ResidualAttention_Inception_subtract_addInitialAttn_clinVarCompress(input_shape,
                                        depth=2,
                                        n_inception=1, # # inception modules after merge
                                        activation='relu',
                                        use_dropout=False,
                                        use_pooling_before_merge=False,
                                        use_pooling_after_merge=False,
                                        use_final_pooling=True,
                                        n_classes=2,
                                        kernel_regularizer = None,
                                        activity_regularizer = None,
                                        n_channels=8,
                                        nLayersForFeatExtr=1):
    
    #Input
    input_left = Input(shape=(input_shape))
    input_right = Input(shape=(input_shape))  
    input_clinical_var = Input(shape=(30,))
    
    #Compress clinical variables
    compress_clinical_var = Dense(5, activation=activation)(input_clinical_var)
    
    
    ####################### ATTENTION ########################################
    attn_network = residualAttentionNetwork(shape=input_shape, 
                                              n_channels=n_channels, 
                                              n_classes=n_classes, 
                                              nLayersForFeatExtr=nLayersForFeatExtr)
    
    # Re-use same instance of base network (weights are shared across both branches)
    processed_left_attn = attn_network(input_left)
    processed_right_attn = attn_network(input_right)

    merged_layer = Subtract()([processed_left_attn, processed_right_attn])
    merged_layer = Concatenate()([merged_layer, processed_left_attn, processed_right_attn])

    # Modules after L-1 merge
    x = Conv3D(n_channels, (3, 3, 3), padding='same')(merged_layer)
    x = BatchNormalization()(x)
    x = Activation('relu')(x)
    x = MaxPool3D(pool_size=(2, 2, 2))(x) 

    x = residual_block(x, input_channels=n_channels, output_channels=64)
    
    x = Flatten()(x)

    

    ####################### INCEPTION ########################################
    base_network = inception_CNN_2_5d(input_shape,
                                    depth=depth,
                                    activation=activation,
                                    use_dropout=use_dropout,
                                    use_pooling=use_pooling_before_merge,
                                    kernel_regularizer = kernel_regularizer,
                                    activity_regularizer = activity_regularizer)



    # Re-use same instance of base network (weights are shared across both branches)
    processed_left = base_network(input_left)
    processed_right = base_network(input_right)
    
    # Maxpooling
    if use_pooling_before_merge:
        processed_left = MaxPooling3D(strides=(2, 2, 2), padding='same')(processed_left)
        processed_right = MaxPooling3D(strides=(2, 2, 2), padding='same')(processed_right)
        
    #Subtract
    merged_layer = Subtract()([processed_left, processed_right])
    
    
    filters = 2
#     kernel_regularizer = regularizers.l1_l2(l1=0.001, l2=0.001)
    kernel_regularizer = None
    
    # Inception module
    tower_1 = Conv3D(filters, kernel_size=(1, 1, 1), padding='same', activation=activation, kernel_regularizer=kernel_regularizer)(merged_layer)
    tower_2 = Conv3D(filters, kernel_size=(5, 5, 5), padding='same', activation=activation, kernel_regularizer=kernel_regularizer)(merged_layer)

    tower_3 = Conv3D(filters, kernel_size=(5, 5, 1), padding='same', activation=activation, kernel_regularizer=kernel_regularizer)(merged_layer)
    
    tower_4 = MaxPooling3D((3, 3, 3), strides=(1, 1, 1), padding='same')(merged_layer)
    tower_4 = Conv3D(filters, kernel_size=(1, 1, 1), padding='same', activation=activation, kernel_regularizer=kernel_regularizer)(tower_4)


    merged_layer = Concatenate(axis=4)([tower_1, tower_2, tower_4])

    
    if use_dropout:
        merged_layer = Dropout(0.05)(merged_layer)

    # Maxpooling
    if use_final_pooling:
        
        merged_layer = MaxPooling3D(strides=(4, 4, 4), padding='same')(merged_layer)
        
    # Flatten
    merged_layer = Flatten()(merged_layer)
    merged_layer = Concatenate()([merged_layer, x, compress_clinical_var])
    
    # Prediction layer
    prediction = Dense(units=n_classes, activation='softmax')(merged_layer)

    return Model(inputs=[input_left, input_right, input_clinical_var], outputs=prediction)


def symmetric_ResidualAttention_Inception_subtract_addInitialAttn_clinVar_semiTrainable(input_shape,
                                        depth=2,
                                        n_inception=1, # # inception modules after merge
                                        activation='relu',
                                        use_dropout=False,
                                        use_pooling_before_merge=False,
                                        use_pooling_after_merge=False,
                                        use_final_pooling=True,
                                        n_classes=2,
                                        kernel_regularizer = None,
                                        activity_regularizer = None,
                                        n_channels=8,
                                        nLayersForFeatExtr=1):
    
    #Input
    input_left = Input(shape=(input_shape))
    input_right = Input(shape=(input_shape))  
    input_clinical_var = Input(shape=(30,))
    
    
    ####################### ATTENTION ########################################
    attn_network = residualAttentionNetwork_semiTrainable(shape=input_shape, 
                                              n_channels=n_channels, 
                                              n_classes=n_classes, 
                                              nLayersForFeatExtr=nLayersForFeatExtr)
    
    # Re-use same instance of base network (weights are shared across both branches)
    processed_left_attn = attn_network(input_left)
    processed_right_attn = attn_network(input_right)

    merged_layer = Subtract()([processed_left_attn, processed_right_attn])
    merged_layer = Concatenate()([merged_layer, processed_left_attn, processed_right_attn])

    # Modules after L-1 merge
    x = Conv3D(n_channels, (3, 3, 3), padding='same')(merged_layer)
    x = BatchNormalization()(x)
    x = Activation('relu')(x)
    x = MaxPool3D(pool_size=(2, 2, 2))(x) 

    x = residual_block(x, input_channels=n_channels, output_channels=64)
    
    x = Flatten()(x)

    

    ####################### INCEPTION ########################################
    base_network = inception_CNN_2_5d_semiTrainable(input_shape,
                                    depth=depth,
                                    activation=activation,
                                    use_dropout=use_dropout,
                                    use_pooling=use_pooling_before_merge,
                                    kernel_regularizer = kernel_regularizer,
                                    activity_regularizer = activity_regularizer)



    # Re-use same instance of base network (weights are shared across both branches)
    processed_left = base_network(input_left)
    processed_right = base_network(input_right)
    
    # Maxpooling
    if use_pooling_before_merge:
        processed_left = MaxPooling3D(strides=(2, 2, 2), padding='same')(processed_left)
        processed_right = MaxPooling3D(strides=(2, 2, 2), padding='same')(processed_right)
        
    #Subtract
    merged_layer = Subtract()([processed_left, processed_right])
    
    
    filters = 2
#     kernel_regularizer = regularizers.l1_l2(l1=0.001, l2=0.001)
    kernel_regularizer = None
    
    # Inception module
    tower_1 = Conv3D(filters, kernel_size=(1, 1, 1), padding='same', activation=activation, kernel_regularizer=kernel_regularizer)(merged_layer)
    tower_2 = Conv3D(filters, kernel_size=(5, 5, 5), padding='same', activation=activation, kernel_regularizer=kernel_regularizer)(merged_layer)

    tower_3 = Conv3D(filters, kernel_size=(5, 5, 1), padding='same', activation=activation, kernel_regularizer=kernel_regularizer)(merged_layer)
    
    tower_4 = MaxPooling3D((3, 3, 3), strides=(1, 1, 1), padding='same')(merged_layer)
    tower_4 = Conv3D(filters, kernel_size=(1, 1, 1), padding='same', activation=activation, kernel_regularizer=kernel_regularizer)(tower_4)


    merged_layer = Concatenate(axis=4)([tower_1, tower_2, tower_4])

    
    if use_dropout:
        merged_layer = Dropout(0.05)(merged_layer)

    # Maxpooling
    if use_final_pooling:
        
        merged_layer = MaxPooling3D(strides=(4, 4, 4), padding='same')(merged_layer)
        
    # Flatten
    merged_layer = Flatten()(merged_layer)
    merged_layer = Concatenate()([merged_layer, x, input_clinical_var])
    
    # Prediction layer
    prediction = Dense(units=n_classes, activation='softmax')(merged_layer)

    return Model(inputs=[input_left, input_right, input_clinical_var], outputs=prediction)

def symmetric_ResidualAttention_Inception_subtract_addInitialAttn_clinVar_onlyLastLayer(input_shape,
                                        depth=2,
                                        n_inception=1, # # inception modules after merge
                                        activation='relu',
                                        use_dropout=False,
                                        use_pooling_before_merge=False,
                                        use_pooling_after_merge=False,
                                        use_final_pooling=True,
                                        n_classes=2,
                                        kernel_regularizer = None,
                                        activity_regularizer = None,
                                        n_channels=8,
                                        nLayersForFeatExtr=1):
    
    #Input
    input_left = Input(shape=(input_shape))
    input_right = Input(shape=(input_shape))  
    input_clinical_var = Input(shape=(30,))
    
    
    ####################### ATTENTION ########################################
    attn_network = residualAttentionNetwork_semiTrainable(shape=input_shape, 
                                              n_channels=n_channels, 
                                              n_classes=n_classes, 
                                              nLayersForFeatExtr=nLayersForFeatExtr)
    
    # Re-use same instance of base network (weights are shared across both branches)
    processed_left_attn = attn_network(input_left)
    processed_right_attn = attn_network(input_right)

    merged_layer = Subtract()([processed_left_attn, processed_right_attn])
    merged_layer = Concatenate()([merged_layer, processed_left_attn, processed_right_attn])

    # Modules after L-1 merge
    x = Conv3D(n_channels, (3, 3, 3), padding='same', trainable=False)(merged_layer)
    x = BatchNormalization()(x)
    x = Activation('relu')(x)
    x = MaxPool3D(pool_size=(2, 2, 2))(x) 

    x = residual_block_semiTrainable(x, input_channels=n_channels, output_channels=64)
    
    x = Flatten()(x)

    

    ####################### INCEPTION ########################################
    base_network = inception_CNN_2_5d_semiTrainable(input_shape,
                                    depth=depth,
                                    activation=activation,
                                    use_dropout=use_dropout,
                                    use_pooling=use_pooling_before_merge,
                                    kernel_regularizer = kernel_regularizer,
                                    activity_regularizer = activity_regularizer)



    # Re-use same instance of base network (weights are shared across both branches)
    processed_left = base_network(input_left)
    processed_right = base_network(input_right)
    
    # Maxpooling
    if use_pooling_before_merge:
        processed_left = MaxPooling3D(strides=(2, 2, 2), padding='same')(processed_left)
        processed_right = MaxPooling3D(strides=(2, 2, 2), padding='same')(processed_right)
        
    #Subtract
    merged_layer = Subtract()([processed_left, processed_right])
    
    
    filters = 2
#     kernel_regularizer = regularizers.l1_l2(l1=0.001, l2=0.001)
    kernel_regularizer = None
    
    # Inception module
    tower_1 = Conv3D(filters, kernel_size=(1, 1, 1), padding='same', activation=activation, kernel_regularizer=kernel_regularizer, trainable=False)(merged_layer)
    tower_2 = Conv3D(filters, kernel_size=(5, 5, 5), padding='same', activation=activation, kernel_regularizer=kernel_regularizer, trainable=False)(merged_layer)

    tower_3 = Conv3D(filters, kernel_size=(5, 5, 1), padding='same', activation=activation, kernel_regularizer=kernel_regularizer, trainable=False)(merged_layer)
    
    tower_4 = MaxPooling3D((3, 3, 3), strides=(1, 1, 1), padding='same')(merged_layer)
    tower_4 = Conv3D(filters, kernel_size=(1, 1, 1), padding='same', activation=activation, kernel_regularizer=kernel_regularizer, trainable=False)(tower_4)


    merged_layer = Concatenate(axis=4)([tower_1, tower_2, tower_4])

    
    if use_dropout:
        merged_layer = Dropout(0.05)(merged_layer)

    # Maxpooling
    if use_final_pooling:
        
        merged_layer = MaxPooling3D(strides=(4, 4, 4), padding='same')(merged_layer)
        
    # Flatten
    merged_layer = Flatten()(merged_layer)
    merged_layer = Concatenate()([merged_layer, x, input_clinical_var])
    
    # Prediction layer
    prediction = Dense(units=n_classes, activation='softmax')(merged_layer)

    return Model(inputs=[input_left, input_right, input_clinical_var], outputs=prediction)


def symmetric_ResidualAttention_Inception_subtract_initattn_2d(input_shape,
                                        depth=2,
                                        n_inception=1, # # inception modules after merge
                                        activation='relu',
                                        use_dropout=False,
                                        use_pooling_before_merge=False,
                                        use_pooling_after_merge=False,
                                        use_final_pooling=True,
                                        n_classes=2,
                                        kernel_regularizer = None,
                                        activity_regularizer = None,
                                        n_channels=8,
                                        nLayersForFeatExtr=1):
    
    #Input
    input_left = Input(shape=(input_shape))
    input_right = Input(shape=(input_shape))    
    
    
    ####################### ATTENTION ########################################
    attn_network = residualAttentionNetwork(shape=input_shape, 
                                              n_channels=n_channels, 
                                              n_classes=n_classes, 
                                              nLayersForFeatExtr=nLayersForFeatExtr)
    
    # Re-use same instance of base network (weights are shared across both branches)
    processed_left_attn = attn_network(input_left)
    processed_right_attn = attn_network(input_right)
    
    
    merged_layer = Subtract()([processed_left_attn, processed_right_attn])
    merged_layer = Concatenate()([merged_layer, processed_left_attn, processed_right_attn])

    # Modules after L-1 merge
    x = Conv3D(n_channels, (3, 3, 3), padding='same')(merged_layer)
    x = BatchNormalization()(x)
    x = Activation('relu')(x)
    x = MaxPool3D(pool_size=(2, 2, 2))(x) 

    x = residual_block(x, input_channels=n_channels, output_channels=64)
    
    x = Flatten()(x)
    

    ####################### INCEPTION ########################################
    base_network = inception_CNN_2_5d_2nd(input_shape,
                                    depth=depth,
                                    activation=activation,
                                    use_dropout=use_dropout,
                                    use_pooling=use_pooling_before_merge,
                                    kernel_regularizer = kernel_regularizer,
                                    activity_regularizer = activity_regularizer)



    # Re-use same instance of base network (weights are shared across both branches)
    processed_left = base_network(input_left)
    processed_right = base_network(input_right)
    
    # Maxpooling
    if use_pooling_before_merge:
        processed_left = MaxPooling3D(strides=(2, 2, 2), padding='same')(processed_left)
        processed_right = MaxPooling3D(strides=(2, 2, 2), padding='same')(processed_right)

    #Subtract
    merged_layer = Subtract()([processed_left, processed_right])
    
    #Concatenate previous images
#     merged_layer = Concatenate()([merged_layer, processed_left, processed_right])
    
    filters = 2

#     kernel_regularizer = regularizers.l1_l2(l1=0.001, l2=0.001)
    kernel_regularizer = None
    
    # Inception module
    tower_1 = Conv3D(filters, kernel_size=(1, 1, 1), padding='same', activation=activation, kernel_regularizer=kernel_regularizer)(merged_layer)
    tower_2 = Conv3D(filters, kernel_size=(5, 5, 1), padding='same', activation=activation, kernel_regularizer=kernel_regularizer)(merged_layer)

#     tower_3 = Conv3D(filters, kernel_size=(5, 5, 1), padding='same', activation=activation, kernel_regularizer=kernel_regularizer)(merged_layer)
    
    tower_4 = MaxPooling3D((3, 3, 1), strides=(1, 1, 1), padding='same')(merged_layer)
    tower_4 = Conv3D(filters, kernel_size=(1, 1, 1), padding='same', activation=activation, kernel_regularizer=kernel_regularizer)(tower_4)


    merged_layer = Concatenate(axis=4)([tower_1, tower_2, tower_4])

    
    if use_dropout:
        merged_layer = Dropout(0.05)(merged_layer)

    # Maxpooling
    if use_final_pooling:
        merged_layer = MaxPooling3D(strides=(2, 2, 2), padding='same')(merged_layer)

    # Flatten
    merged_layer = Flatten()(merged_layer)
    merged_layer = Concatenate()([merged_layer, x])
    
    
    #Dropout
#     merged_layer = Dropout(0.05)(merged_layer)
    
    # Prediction layer
    prediction = Dense(units=n_classes, activation='softmax')(merged_layer)


    return Model(inputs=[input_left, input_right], outputs=prediction)

def symmetric_ResidualAttention_Inception_subtract_addInitialAttn_reduceDim(input_shape,
                                        depth=2,
                                        n_inception=1, # # inception modules after merge
                                        activation='relu',
                                        use_dropout=False,
                                        use_pooling_before_merge=False,
                                        use_pooling_after_merge=False,
                                        use_final_pooling=True,
                                        n_classes=2,
                                        kernel_regularizer = None,
                                        activity_regularizer = None,
                                        n_channels=8,
                                        nLayersForFeatExtr=1):
    
    #Input
    input_left = Input(shape=(input_shape))
    input_right = Input(shape=(input_shape))    
    
    
    ####################### ATTENTION ########################################
    attn_network = residualAttentionNetwork(shape=input_shape, 
                                              n_channels=n_channels, 
                                              n_classes=n_classes, 
                                              nLayersForFeatExtr=nLayersForFeatExtr)
    
    # Re-use same instance of base network (weights are shared across both branches)
    processed_left_attn = attn_network(input_left)
    processed_right_attn = attn_network(input_right)

    merged_layer = Subtract()([processed_left_attn, processed_right_attn])
    merged_layer = Concatenate()([merged_layer, processed_left_attn, processed_right_attn])

    # Modules after L-1 merge
    x = Conv3D(n_channels, (3, 3, 3), padding='same')(merged_layer)
    x = BatchNormalization()(x)
    x = Activation('relu')(x)
    x = MaxPool3D(pool_size=(2, 2, 2))(x) 
    
#     x = MaxPool3D(pool_size=(4,4,4))(x) 

    x = residual_block(x, input_channels=n_channels, output_channels=64)
#     x = MaxPool3D(pool_size=(2,2,2))(x) #test168
    
    x = Flatten()(x)
#     x = Dense(16)(x) #test168

    ####################### INCEPTION ########################################
    base_network = inception_CNN_2_5d(input_shape,
                                    depth=depth,
                                    activation=activation,
                                    use_dropout=use_dropout,
                                    use_pooling=use_pooling_before_merge,
                                    kernel_regularizer = kernel_regularizer,
                                    activity_regularizer = activity_regularizer)



    # Re-use same instance of base network (weights are shared across both branches)
    processed_left = base_network(input_left)
    processed_right = base_network(input_right)
    
    # Maxpooling
    if use_pooling_before_merge:
        processed_left = MaxPooling3D(strides=(2, 2, 2), padding='same')(processed_left)
        processed_right = MaxPooling3D(strides=(2, 2, 2), padding='same')(processed_right)
        
    #Subtract
    merged_layer = Subtract()([processed_left, processed_right])
    

    filters = 2
#     kernel_regularizer = None
#     kernel_regularizer = regularizers.l1_l2(l1=0.001, l2=0.001)
    ########## ORIGINAL INCEPTION ##########
    # Inception module
    tower_1 = Conv3D(filters, kernel_size=(1, 1, 1), padding='same', activation=activation, kernel_regularizer=kernel_regularizer)(merged_layer)
    tower_2 = Conv3D(filters, kernel_size=(5, 5, 5), padding='same', activation=activation, kernel_regularizer=kernel_regularizer)(merged_layer)
#     tower_2 = Conv3D(filters, kernel_size=(3,3,3), padding='same', activation=activation, kernel_regularizer=kernel_regularizer)(merged_layer)

    tower_4 = MaxPooling3D((3, 3, 3), strides=(1, 1, 1), padding='same')(merged_layer)
    tower_4 = Conv3D(filters, kernel_size=(1, 1, 1), padding='same', activation=activation, kernel_regularizer=kernel_regularizer)(tower_4)

#     ########## ALTERNATE INCEPTION ##########
#      # Inception module
#     tower_1 = Conv3D(filters, kernel_size=(5, 1, 1), padding='same', activation=activation, kernel_regularizer=kernel_regularizer)(merged_layer)
#     tower_2 = Conv3D(filters, kernel_size=(1, 5, 1), padding='same', activation=activation, kernel_regularizer=kernel_regularizer)(merged_layer)
    
#     tower_4 = MaxPooling3D((3, 3, 3), strides=(1, 1, 1), padding='same')(merged_layer)
#     tower_4 = Conv3D(filters, kernel_size=(1, 1, 5), padding='same', activation=activation, kernel_regularizer=kernel_regularizer)(tower_4)

    #1D 5,1,1 1,5,1 1,1,5
    #2D 1,5,5 5,5,1 5,1,5

    merged_layer = Concatenate(axis=4)([tower_1, tower_2, tower_4])

    
    if use_dropout:
        merged_layer = Dropout(0.05)(merged_layer)

    # Maxpooling
    if use_final_pooling:
        
        merged_layer = MaxPooling3D(strides=(4, 4, 4), padding='same')(merged_layer)
#         merged_layer = MaxPooling3D(strides=(8,8,8), padding='same')(merged_layer) #test166
#         merged_layer = AveragePooling3D(strides=(2,2,2), padding='same')(merged_layer) #test168
        

    # Flatten
    merged_layer = Flatten()(merged_layer)
#     merged_layer = Dense(16)(merged_layer) #test168
    merged_layer = Concatenate()([merged_layer, x])
#     merged_layer = Dense(512)(merged_layer) 
#     merged_layer = Dense(256)(merged_layer) 
#     merged_layer = Dense(128)(merged_layer) 
#     merged_layer = Dropout(0.1)(merged_layer)
#     merged_layer = BatchNormalization()(merged_layer)
#     merged_layer = Activation('elu')(merged_layer)
    
#     merged_layer = Dense(64)(merged_layer) 
#     merged_layer = Dropout(0.1)(merged_layer)
#     merged_layer = BatchNormalization()(merged_layer)
#     merged_layer = Activation('elu')(merged_layer)
#     merged_layer = Dense(32)(merged_layer) 
#     merged_layer = Dense(32)(merged_layer) 
#     merged_layer = Dense(8)(merged_layer) 
#     merged_layer = Dense(128)(x) #test165
    
    #Dropout
#     merged_layer = Dropout(0.05)(merged_layer)
    
    # Prediction layer
    prediction = Dense(units=n_classes, activation='softmax')(merged_layer) ##ORIGINAL (MAKES NETWORK LARGER)
#     prediction = Dense(units=1, activation='sigmoid')(merged_layer) ##MAKES NETWORK SMALLER


    return Model(inputs=[input_left, input_right], outputs=prediction)

def symmetric_ResidualAttention_Inception_subtract_addInitialAttn_reduceDim_allCrossSectional(input_shape,
                                        depth=2,
                                        n_inception=1, # # inception modules after merge
                                        activation='relu',
                                        use_dropout=False,
                                        use_pooling_before_merge=False,
                                        use_pooling_after_merge=False,
                                        use_final_pooling=True,
                                        n_classes=2,
                                        kernel_regularizer = None,
                                        activity_regularizer = None,
                                        n_channels=8,
                                        nLayersForFeatExtr=1):
    
    #Input
    input_left = Input(shape=(input_shape))
    input_right = Input(shape=(input_shape))    
    
    
    ####################### ATTENTION ########################################
    attn_network = residualAttentionNetwork(shape=input_shape, 
                                              n_channels=n_channels, 
                                              n_classes=n_classes, 
                                              nLayersForFeatExtr=nLayersForFeatExtr)
    
    # Re-use same instance of base network (weights are shared across both branches)
    processed_left_attn = attn_network(input_left)
    processed_right_attn = attn_network(input_right)

    merged_layer = Subtract()([processed_left_attn, processed_right_attn])
    merged_layer = Concatenate()([merged_layer, processed_left_attn, processed_right_attn])

    # Modules after L-1 merge
    x = Conv3D(n_channels, (3, 3, 3), padding='same')(merged_layer)
    x = BatchNormalization()(x)
    x = Activation('relu')(x)
    x = MaxPool3D(pool_size=(2, 2, 2))(x) 
    
#     x = MaxPool3D(pool_size=(4,4,4))(x) 

    x = residual_block(x, input_channels=n_channels, output_channels=64)
#     x = MaxPool3D(pool_size=(2,2,2))(x) #test168
    
    x = Flatten()(x)
#     x = Dense(16)(x) #test168

    ####################### INCEPTION ########################################
    base_network = inception_CNN_2_5d(input_shape,
                                    depth=depth,
                                    activation=activation,
                                    use_dropout=use_dropout,
                                    use_pooling=use_pooling_before_merge,
                                    kernel_regularizer = kernel_regularizer,
                                    activity_regularizer = activity_regularizer)



    # Re-use same instance of base network (weights are shared across both branches)
    processed_left = base_network(input_left)
    processed_right = base_network(input_right)
    
    # Maxpooling
    if use_pooling_before_merge:
        processed_left = MaxPooling3D(strides=(2, 2, 2), padding='same')(processed_left)
        processed_right = MaxPooling3D(strides=(2, 2, 2), padding='same')(processed_right)
        
    #Subtract
    merged_layer = Subtract()([processed_left, processed_right])
    merged_layer = Concatenate()([merged_layer, processed_left, processed_right])
    

    filters = 2
#     kernel_regularizer = None
#     kernel_regularizer = regularizers.l1_l2(l1=0.001, l2=0.001)
    ########## ORIGINAL INCEPTION ##########
    # Inception module
    tower_1 = Conv3D(filters, kernel_size=(1, 1, 1), padding='same', activation=activation, kernel_regularizer=kernel_regularizer)(merged_layer)
    tower_2 = Conv3D(filters, kernel_size=(5, 5, 5), padding='same', activation=activation, kernel_regularizer=kernel_regularizer)(merged_layer)
#     tower_2 = Conv3D(filters, kernel_size=(3,3,3), padding='same', activation=activation, kernel_regularizer=kernel_regularizer)(merged_layer)

    tower_4 = MaxPooling3D((3, 3, 3), strides=(1, 1, 1), padding='same')(merged_layer)
    tower_4 = Conv3D(filters, kernel_size=(1, 1, 1), padding='same', activation=activation, kernel_regularizer=kernel_regularizer)(tower_4)

#     ########## ALTERNATE INCEPTION ##########
#      # Inception module
#     tower_1 = Conv3D(filters, kernel_size=(5, 1, 1), padding='same', activation=activation, kernel_regularizer=kernel_regularizer)(merged_layer)
#     tower_2 = Conv3D(filters, kernel_size=(1, 5, 1), padding='same', activation=activation, kernel_regularizer=kernel_regularizer)(merged_layer)
    
#     tower_4 = MaxPooling3D((3, 3, 3), strides=(1, 1, 1), padding='same')(merged_layer)
#     tower_4 = Conv3D(filters, kernel_size=(1, 1, 5), padding='same', activation=activation, kernel_regularizer=kernel_regularizer)(tower_4)

    #1D 5,1,1 1,5,1 1,1,5
    #2D 1,5,5 5,5,1 5,1,5

    merged_layer = Concatenate(axis=4)([tower_1, tower_2, tower_4])

    
    if use_dropout:
        merged_layer = Dropout(0.05)(merged_layer)

    # Maxpooling
    if use_final_pooling:
        
        merged_layer = MaxPooling3D(strides=(4, 4, 4), padding='same')(merged_layer)
#         merged_layer = MaxPooling3D(strides=(8,8,8), padding='same')(merged_layer) #test166
#         merged_layer = AveragePooling3D(strides=(2,2,2), padding='same')(merged_layer) #test168
        

    # Flatten
    merged_layer = Flatten()(merged_layer)
#     merged_layer = Dense(16)(merged_layer) #test168
    merged_layer = Concatenate()([merged_layer, x])
#     merged_layer = Dense(512)(merged_layer) 
#     merged_layer = Dense(256)(merged_layer) 
#     merged_layer = Dense(128)(merged_layer) 
#     merged_layer = Dropout(0.1)(merged_layer)
#     merged_layer = BatchNormalization()(merged_layer)
#     merged_layer = Activation('elu')(merged_layer)
    
#     merged_layer = Dense(64)(merged_layer) 
#     merged_layer = Dropout(0.1)(merged_layer)
#     merged_layer = BatchNormalization()(merged_layer)
#     merged_layer = Activation('elu')(merged_layer)
#     merged_layer = Dense(32)(merged_layer) 
#     merged_layer = Dense(32)(merged_layer) 
#     merged_layer = Dense(8)(merged_layer) 
#     merged_layer = Dense(128)(x) #test165
    
    #Dropout
#     merged_layer = Dropout(0.05)(merged_layer)
    
    # Prediction layer
    prediction = Dense(units=n_classes, activation='softmax')(merged_layer) ##ORIGINAL (MAKES NETWORK LARGER)
#     prediction = Dense(units=1, activation='sigmoid')(merged_layer) ##MAKES NETWORK SMALLER


    return Model(inputs=[input_left, input_right], outputs=prediction)

def symmetric_ResidualAttention_Inception_subtract_addInitialAttn_clinVar(input_shape,
                                        depth=2,
                                        n_inception=1, # # inception modules after merge
                                        activation='relu',
                                        use_dropout=False,
                                        use_pooling_before_merge=False,
                                        use_pooling_after_merge=False,
                                        use_final_pooling=True,
                                        n_classes=2,
                                        kernel_regularizer = None,
                                        activity_regularizer = None,
                                        n_channels=8,
                                        nLayersForFeatExtr=1):
    
    #Input
    input_left = Input(shape=(input_shape))
    input_right = Input(shape=(input_shape))  
    input_clinical_var = Input(shape=(30,))
    
    
    ####################### ATTENTION ########################################
    attn_network = residualAttentionNetwork(shape=input_shape, 
                                              n_channels=n_channels, 
                                              n_classes=n_classes, 
                                              nLayersForFeatExtr=nLayersForFeatExtr)
    
    # Re-use same instance of base network (weights are shared across both branches)
    processed_left_attn = attn_network(input_left)
    processed_right_attn = attn_network(input_right)

    merged_layer = Subtract()([processed_left_attn, processed_right_attn])
    merged_layer = Concatenate()([merged_layer, processed_left_attn, processed_right_attn])

    # Modules after L-1 merge
    x = Conv3D(n_channels, (3, 3, 3), padding='same')(merged_layer)
    x = BatchNormalization()(x)
    x = Activation('relu')(x)
    x = MaxPool3D(pool_size=(2, 2, 2))(x) 

    x = residual_block(x, input_channels=n_channels, output_channels=64)
    
    x = Flatten()(x)

    ####################### INCEPTION ########################################
    base_network = inception_CNN_2_5d(input_shape,
                                    depth=depth,
                                    activation=activation,
                                    use_dropout=use_dropout,
                                    use_pooling=use_pooling_before_merge,
                                    kernel_regularizer = kernel_regularizer,
                                    activity_regularizer = activity_regularizer)



    # Re-use same instance of base network (weights are shared across both branches)
    processed_left = base_network(input_left)
    processed_right = base_network(input_right)
    
    # Maxpooling
    if use_pooling_before_merge:
        processed_left = MaxPooling3D(strides=(2, 2, 2), padding='same')(processed_left)
        processed_right = MaxPooling3D(strides=(2, 2, 2), padding='same')(processed_right)
        
    #Subtract
    merged_layer = Subtract()([processed_left, processed_right])
    

    filters = 2
    kernel_regularizer = None
#     kernel_regularizer = regularizers.l1_l2(l1=0.001, l2=0.001)
    ########## ORIGINAL INCEPTION ##########
    # Inception module
    tower_1 = Conv3D(filters, kernel_size=(1, 1, 1), padding='same', activation=activation, kernel_regularizer=kernel_regularizer)(merged_layer)
    tower_2 = Conv3D(filters, kernel_size=(5, 5, 5), padding='same', activation=activation, kernel_regularizer=kernel_regularizer)(merged_layer)

    tower_4 = MaxPooling3D((3, 3, 3), strides=(1, 1, 1), padding='same')(merged_layer)
    tower_4 = Conv3D(filters, kernel_size=(1, 1, 1), padding='same', activation=activation, kernel_regularizer=kernel_regularizer)(tower_4)

#     ########## ALTERNATE INCEPTION ##########
#      # Inception module
#     tower_1 = Conv3D(filters, kernel_size=(5, 1, 1), padding='same', activation=activation, kernel_regularizer=kernel_regularizer)(merged_layer)
#     tower_2 = Conv3D(filters, kernel_size=(1, 5, 1), padding='same', activation=activation, kernel_regularizer=kernel_regularizer)(merged_layer)
    
#     tower_4 = MaxPooling3D((3, 3, 3), strides=(1, 1, 1), padding='same')(merged_layer)
#     tower_4 = Conv3D(filters, kernel_size=(1, 1, 5), padding='same', activation=activation, kernel_regularizer=kernel_regularizer)(tower_4)

    #1D 5,1,1 1,5,1 1,1,5
    #2D 1,5,5 5,5,1 5,1,5

    merged_layer = Concatenate(axis=4)([tower_1, tower_2, tower_4])

    
    if use_dropout:
        merged_layer = Dropout(0.05)(merged_layer)

    # Maxpooling
    if use_final_pooling:
        
        merged_layer = MaxPooling3D(strides=(4, 4, 4), padding='same')(merged_layer)
        
    # Flatten
    merged_layer = Flatten()(merged_layer)
    merged_layer = Concatenate()([merged_layer, x, input_clinical_var])
    
    # Prediction layer
    prediction = Dense(units=n_classes, activation='softmax')(merged_layer)

    return Model(inputs=[input_left, input_right, input_clinical_var], outputs=prediction)

def symmetric_InceptionOnly(input_shape,
                                        depth=2,
                                        n_inception=1, # # inception modules after merge
                                        activation='relu',
                                        use_dropout=False,
                                        use_pooling_before_merge=False,
                                        use_pooling_after_merge=False,
                                        use_final_pooling=True,
                                        n_classes=2,
                                        kernel_regularizer = None,
                                        activity_regularizer = None,
                                        n_channels=8,
                                        nLayersForFeatExtr=1):
    
    #Input
    input_left = Input(shape=(input_shape))
    input_right = Input(shape=(input_shape))  
    input_clinical_var = Input(shape=(30,))
    
    ####################### INCEPTION ########################################
    base_network = inception_CNN_2_5d(input_shape,
                                    depth=depth,
                                    activation=activation,
                                    use_dropout=use_dropout,
                                    use_pooling=use_pooling_before_merge,
                                    kernel_regularizer = kernel_regularizer,
                                    activity_regularizer = activity_regularizer)



    # Re-use same instance of base network (weights are shared across both branches)
    processed_left = base_network(input_left)
    processed_right = base_network(input_right)
    
    # Maxpooling
    if use_pooling_before_merge:
        processed_left = MaxPooling3D(strides=(2, 2, 2), padding='same')(processed_left)
        processed_right = MaxPooling3D(strides=(2, 2, 2), padding='same')(processed_right)
        
    #Subtract
    merged_layer = Subtract()([processed_left, processed_right])
    

    filters = 2
    kernel_regularizer = None
    ########## ORIGINAL INCEPTION ##########
    # Inception module
    tower_1 = Conv3D(filters, kernel_size=(1, 1, 1), padding='same', activation=activation, kernel_regularizer=kernel_regularizer)(merged_layer)
    tower_2 = Conv3D(filters, kernel_size=(5, 5, 5), padding='same', activation=activation, kernel_regularizer=kernel_regularizer)(merged_layer)

    tower_4 = MaxPooling3D((3, 3, 3), strides=(1, 1, 1), padding='same')(merged_layer)
    tower_4 = Conv3D(filters, kernel_size=(1, 1, 1), padding='same', activation=activation, kernel_regularizer=kernel_regularizer)(tower_4)


    merged_layer = Concatenate(axis=4)([tower_1, tower_2, tower_4])
    
    # Maxpooling
    if use_final_pooling:
        
        merged_layer = MaxPooling3D(strides=(2, 3, 2), padding='same')(merged_layer)

    # Flatten
    merged_layer = Flatten()(merged_layer)
    
    # Prediction layer
    prediction = Dense(units=n_classes, activation='softmax')(merged_layer)

    return Model(inputs=[input_left, input_right, input_clinical_var], outputs=prediction)


def symmetric_ResidualAttention_only(input_shape,
                                        depth=2,
                                        n_inception=1, # # inception modules after merge
                                        activation='relu',
                                        use_dropout=False,
                                        use_pooling_before_merge=False,
                                        use_pooling_after_merge=False,
                                        use_final_pooling=True,
                                        n_classes=2,
                                        kernel_regularizer = None,
                                        activity_regularizer = None,
                                        n_channels=8,
                                        nLayersForFeatExtr=1):
    
    #Input
    input_left = Input(shape=(input_shape))
    input_right = Input(shape=(input_shape))  
    input_clinical_var = Input(shape=(30,))
    
    
    ####################### ATTENTION ########################################
    attn_network = residualAttentionNetwork(shape=input_shape, 
                                              n_channels=n_channels, 
                                              n_classes=n_classes, 
                                              nLayersForFeatExtr=nLayersForFeatExtr)
    
    # Re-use same instance of base network (weights are shared across both branches)
    processed_left_attn = attn_network(input_left)
    processed_right_attn = attn_network(input_right)

    merged_layer = Subtract()([processed_left_attn, processed_right_attn])
    merged_layer = Concatenate()([merged_layer, processed_left_attn, processed_right_attn])

    # Modules after L-1 merge
    x = Conv3D(n_channels, (3, 3, 3), padding='same')(merged_layer)
    x = BatchNormalization()(x)
    x = Activation('relu')(x)
    x = MaxPool3D(pool_size=(2, 1, 2))(x) 

    x = residual_block(x, input_channels=n_channels, output_channels=64)
    
    x = Flatten()(x)
    
    # Prediction layer
    prediction = Dense(units=n_classes, activation='softmax')(x)

    return Model(inputs=[input_left, input_right, input_clinical_var], outputs=prediction)

def symmetric_ResidualAttention_Inception_subtract_addInitialAttn_reduceDim_multilabel_multiclass(input_shape,
                                        depth=2,
                                        n_inception=1, # # inception modules after merge
                                        activation='relu',
                                        use_dropout=False,
                                        use_pooling_before_merge=False,
                                        use_pooling_after_merge=False,
                                        use_final_pooling=True,
                                        n_classes=2,
                                        kernel_regularizer = None,
                                        activity_regularizer = None,
                                        n_channels=8,
                                        nLayersForFeatExtr=1):
    
    #Input
    input_left = Input(shape=(input_shape))
    input_right = Input(shape=(input_shape))    
    
    
    ####################### ATTENTION ########################################
    attn_network = residualAttentionNetwork(shape=input_shape, 
                                              n_channels=n_channels, 
                                              n_classes=n_classes, 
                                              nLayersForFeatExtr=nLayersForFeatExtr)
    
    # Re-use same instance of base network (weights are shared across both branches)
    processed_left_attn = attn_network(input_left)
    processed_right_attn = attn_network(input_right)

    merged_layer = Subtract()([processed_left_attn, processed_right_attn])
    merged_layer = Concatenate()([merged_layer, processed_left_attn, processed_right_attn])

    # Modules after L-1 merge
    x = Conv3D(n_channels, (3, 3, 3), padding='same')(merged_layer)
    x = BatchNormalization()(x)
    x = Activation('relu')(x)
#     x = MaxPool3D(pool_size=(2, 2, 2))(x) 
    
    x = MaxPool3D(pool_size=(4,4,4))(x) 

    x = residual_block(x, input_channels=n_channels, output_channels=64)
#     x = MaxPool3D(pool_size=(2,2,2))(x) #test168
    
    x = Flatten()(x)
#     x = Dense(16)(x) #test168

    ####################### INCEPTION ########################################
    base_network = inception_CNN_2_5d(input_shape,
                                    depth=depth,
                                    activation=activation,
                                    use_dropout=use_dropout,
                                    use_pooling=use_pooling_before_merge,
                                    kernel_regularizer = kernel_regularizer,
                                    activity_regularizer = activity_regularizer)



    # Re-use same instance of base network (weights are shared across both branches)
    processed_left = base_network(input_left)
    processed_right = base_network(input_right)
    
    # Maxpooling
    if use_pooling_before_merge:
        processed_left = MaxPooling3D(strides=(2, 2, 2), padding='same')(processed_left)
        processed_right = MaxPooling3D(strides=(2, 2, 2), padding='same')(processed_right)
        
    #Subtract
    merged_layer = Subtract()([processed_left, processed_right])
    

    filters = 2
#     kernel_regularizer = None
    kernel_regularizer = regularizers.l1_l2(l1=0.001, l2=0.001)
#     ########## ORIGINAL INCEPTION ##########
#     # Inception module
#     tower_1 = Conv3D(filters, kernel_size=(1, 1, 1), padding='same', activation=activation, kernel_regularizer=kernel_regularizer)(merged_layer)
#     tower_2 = Conv3D(filters, kernel_size=(5, 5, 5), padding='same', activation=activation, kernel_regularizer=kernel_regularizer)(merged_layer)

#     tower_4 = MaxPooling3D((3, 3, 3), strides=(1, 1, 1), padding='same')(merged_layer)
#     tower_4 = Conv3D(filters, kernel_size=(1, 1, 1), padding='same', activation=activation, kernel_regularizer=kernel_regularizer)(tower_4)

    ########## ALTERNATE INCEPTION ##########
     # Inception module
    tower_1 = Conv3D(filters, kernel_size=(5, 1, 1), padding='same', activation=activation, kernel_regularizer=kernel_regularizer)(merged_layer)
    tower_2 = Conv3D(filters, kernel_size=(1, 5, 1), padding='same', activation=activation, kernel_regularizer=kernel_regularizer)(merged_layer)
    
    tower_4 = MaxPooling3D((3, 3, 3), strides=(1, 1, 1), padding='same')(merged_layer)
    tower_4 = Conv3D(filters, kernel_size=(1, 1, 5), padding='same', activation=activation, kernel_regularizer=kernel_regularizer)(tower_4)

    #1D 5,1,1 1,5,1 1,1,5
    #2D 1,5,5 5,5,1 5,1,5

    merged_layer = Concatenate(axis=4)([tower_1, tower_2, tower_4])

    
    if use_dropout:
        merged_layer = Dropout(0.05)(merged_layer)

    # Maxpooling
    if use_final_pooling:
        
#         merged_layer = MaxPooling3D(strides=(4, 4, 4), padding='same')(merged_layer)
        merged_layer = MaxPooling3D(strides=(8,8,8), padding='same')(merged_layer) #test166
#         merged_layer = AveragePooling3D(strides=(2,2,2), padding='same')(merged_layer) #test168
        

    # Flatten
    merged_layer = Flatten()(merged_layer)
#     merged_layer = Dense(16)(merged_layer) #test168
    merged_layer = Concatenate()([merged_layer, x])
#     merged_layer = Dense(512)(merged_layer) 
#     merged_layer = Dense(256)(merged_layer) 
#     merged_layer = Dense(128)(merged_layer) 
#     merged_layer = Dropout(0.1)(merged_layer)
#     merged_layer = BatchNormalization()(merged_layer)
#     merged_layer = Activation('elu')(merged_layer)
    
    merged_layer = Dense(64)(merged_layer) 
#     merged_layer = Dropout(0.1)(merged_layer)
#     merged_layer = BatchNormalization()(merged_layer)
#     merged_layer = Activation('elu')(merged_layer)
#     merged_layer = Dense(32)(merged_layer) 
#     merged_layer = Dense(32)(merged_layer) 
#     merged_layer = Dense(8)(merged_layer) 
#     merged_layer = Dense(128)(x) #test165
    
    #Dropout
#     merged_layer = Dropout(0.05)(merged_layer)
    
    # Prediction layer
#     prediction = Dense(units=n_classes, activation='softmax')(merged_layer) ##ORIGINAL (MAKES NETWORK LARGER)
    prediction = Dense(units=9, activation='sigmoid', name='multiLabel_multiClass')(merged_layer) ##MAKES NETWORK SMALLER
#     prediction2 = Dense(units=1, activation='sigmoid', name='APOE4')(merged_layer) ##MAKES NETWORK SMALLER
#     prediction3 = Dense(units=1, activation='sigmoid', name='ADAS11')(merged_layer) ##MAKES NETWORK SMALLER
#     prediction4 = Dense(units=1, activation='sigmoid', name='ADAS13')(merged_layer) ##MAKES NETWORK SMALLER
#     prediction5 = Dense(units=1, activation='sigmoid', name='ADASQ4')(merged_layer) ##MAKES NETWORK SMALLER
#     prediction6 = Dense(units=1, activation='sigmoid', name='MMSE')(merged_layer) ##MAKES NETWORK SMALLER
#     prediction7 = Dense(units=1, activation='sigmoid', name='RAVLT')(merged_layer) ##MAKES NETWORK SMALLER
#     prediction8 = Dense(units=1, activation='sigmoid', name='FAQ')(merged_layer) ##MAKES NETWORK SMALLER
#     prediction9 = Dense(units=1, activation='sigmoid', name='MOCA')(merged_layer) ##MAKES NETWORK SMALLER
    
#     prediction = [[prediction1], [prediction2], [prediction3], [prediction4], [prediction5], [prediction6], [prediction7], [prediction8], [prediction9]]
#     Concatenate()([prediction1, prediction2, prediction3, prediction4, prediction5, prediction6, prediction7, prediction8, prediction9])

    return Model(inputs=[input_left, input_right], outputs=prediction)


def symmetric_ResidualAttention_Inception_subtract_addInitialAttn_reduceDim_multilabel_multiclass_hier(input_shape,
                                        depth=2,
                                        n_inception=1, # # inception modules after merge
                                        activation='relu',
                                        use_dropout=False,
                                        use_pooling_before_merge=False,
                                        use_pooling_after_merge=False,
                                        use_final_pooling=True,
                                        n_classes=2,
                                        kernel_regularizer = None,
                                        activity_regularizer = None,
                                        n_channels=8,
                                        nLayersForFeatExtr=1):
    
    #Input
    input_left = Input(shape=(input_shape))
    input_right = Input(shape=(input_shape))    
    
    
    ####################### ATTENTION ########################################
    attn_network = residualAttentionNetwork(shape=input_shape, 
                                              n_channels=n_channels, 
                                              n_classes=n_classes, 
                                              nLayersForFeatExtr=nLayersForFeatExtr)
    
    # Re-use same instance of base network (weights are shared across both branches)
    processed_left_attn = attn_network(input_left)
    processed_right_attn = attn_network(input_right)

    merged_layer = Subtract()([processed_left_attn, processed_right_attn])
    merged_layer = Concatenate()([merged_layer, processed_left_attn, processed_right_attn])

    # Modules after L-1 merge
    x = Conv3D(n_channels, (3, 3, 3), padding='same')(merged_layer)
    x = BatchNormalization()(x)
    x = Activation('relu')(x)
#     x = MaxPool3D(pool_size=(2, 2, 2))(x) 
    
    x = MaxPool3D(pool_size=(4,4,4))(x) 

    x = residual_block(x, input_channels=n_channels, output_channels=64)
#     x = MaxPool3D(pool_size=(2,2,2))(x) #test168
    
    x = Flatten()(x)
#     x = Dense(16)(x) #test168

    ####################### INCEPTION ########################################
    base_network = inception_CNN_2_5d(input_shape,
                                    depth=depth,
                                    activation=activation,
                                    use_dropout=use_dropout,
                                    use_pooling=use_pooling_before_merge,
                                    kernel_regularizer = kernel_regularizer,
                                    activity_regularizer = activity_regularizer)



    # Re-use same instance of base network (weights are shared across both branches)
    processed_left = base_network(input_left)
    processed_right = base_network(input_right)
    
    # Maxpooling
    if use_pooling_before_merge:
        processed_left = MaxPooling3D(strides=(2, 2, 2), padding='same')(processed_left)
        processed_right = MaxPooling3D(strides=(2, 2, 2), padding='same')(processed_right)
        
    #Subtract
    merged_layer = Subtract()([processed_left, processed_right])
    

    filters = 2
#     kernel_regularizer = None
    kernel_regularizer = regularizers.l1_l2(l1=0.001, l2=0.001)
#     ########## ORIGINAL INCEPTION ##########
#     # Inception module
#     tower_1 = Conv3D(filters, kernel_size=(1, 1, 1), padding='same', activation=activation, kernel_regularizer=kernel_regularizer)(merged_layer)
#     tower_2 = Conv3D(filters, kernel_size=(5, 5, 5), padding='same', activation=activation, kernel_regularizer=kernel_regularizer)(merged_layer)

#     tower_4 = MaxPooling3D((3, 3, 3), strides=(1, 1, 1), padding='same')(merged_layer)
#     tower_4 = Conv3D(filters, kernel_size=(1, 1, 1), padding='same', activation=activation, kernel_regularizer=kernel_regularizer)(tower_4)

    ########## ALTERNATE INCEPTION ##########
     # Inception module
    tower_1 = Conv3D(filters, kernel_size=(5, 1, 1), padding='same', activation=activation, kernel_regularizer=kernel_regularizer)(merged_layer)
    tower_2 = Conv3D(filters, kernel_size=(1, 5, 1), padding='same', activation=activation, kernel_regularizer=kernel_regularizer)(merged_layer)
    
    tower_4 = MaxPooling3D((3, 3, 3), strides=(1, 1, 1), padding='same')(merged_layer)
    tower_4 = Conv3D(filters, kernel_size=(1, 1, 5), padding='same', activation=activation, kernel_regularizer=kernel_regularizer)(tower_4)

    #1D 5,1,1 1,5,1 1,1,5
    #2D 1,5,5 5,5,1 5,1,5

    merged_layer = Concatenate(axis=4)([tower_1, tower_2, tower_4])

    
    if use_dropout:
        merged_layer = Dropout(0.05)(merged_layer)

    # Maxpooling
    if use_final_pooling:
        
#         merged_layer = MaxPooling3D(strides=(4, 4, 4), padding='same')(merged_layer)
        merged_layer = MaxPooling3D(strides=(8,8,8), padding='same')(merged_layer) #test166
#         merged_layer = AveragePooling3D(strides=(2,2,2), padding='same')(merged_layer) #test168
        

    # Flatten
    merged_layer = Flatten()(merged_layer)
    merged_layer = Concatenate()([merged_layer, x])

    merged_layer = Dense(64)(merged_layer) 

    
    # Prediction layer
#     prediction = Dense(units=n_classes, activation='softmax')(merged_layer) ##ORIGINAL (MAKES NETWORK LARGER)
#     prediction = Dense(units=9, activation='sigmoid', name='multiLabel_multiClass')(merged_layer) ##MAKES NETWORK SMALLER
    prediction1 = Dense(units=32,  name='CDRSB')(merged_layer) ##MAKES NETWORK SMALLER
    prediction2 = Dense(units=32, name='APOE4')(merged_layer) ##MAKES NETWORK SMALLER
    prediction3 = Dense(units=32, name='ADAS11')(merged_layer) ##MAKES NETWORK SMALLER
    prediction4 = Dense(units=32, name='ADAS13')(merged_layer) ##MAKES NETWORK SMALLER
    prediction5 = Dense(units=32, name='ADASQ4')(merged_layer) ##MAKES NETWORK SMALLER
    prediction6 = Dense(units=32, name='MMSE')(merged_layer) ##MAKES NETWORK SMALLER
    prediction7 = Dense(units=32, name='RAVLT')(merged_layer) ##MAKES NETWORK SMALLER
    prediction8 = Dense(units=32, name='FAQ')(merged_layer) ##MAKES NETWORK SMALLER
    prediction9 = Dense(units=32, name='MOCA')(merged_layer) ##MAKES NETWORK SMALLER
    
    merged_layer = Concatenate()([prediction1, prediction2, prediction3, prediction4, prediction5, prediction6, prediction7, prediction8, prediction9])
    
    prediction = Dense(units=9, activation='sigmoid', name='multiLabel_multiClass')(merged_layer) ##MAKES NETWORK SMALLER
    
#     prediction = [[prediction1], [prediction2], [prediction3], [prediction4], [prediction5], [prediction6], [prediction7], [prediction8], [prediction9]]


    return Model(inputs=[input_left, input_right], outputs=prediction)

def symmetric_ResidualAttention_Inception_subtract_addInitialAttn_includeAD(input_shape,
                                        depth=2,
                                        n_inception=1, # # inception modules after merge
                                        activation='relu',
                                        use_dropout=False,
                                        use_pooling_before_merge=False,
                                        use_pooling_after_merge=False,
                                        use_final_pooling=True,
                                        n_classes=2,
                                        kernel_regularizer = None,
                                        activity_regularizer = None,
                                        n_channels=8,
                                        nLayersForFeatExtr=1):
    
    #Input
    input_left = Input(shape=(input_shape))
    input_right = Input(shape=(input_shape))    
    
    
    ####################### ATTENTION ########################################
    attn_network = residualAttentionNetwork(shape=input_shape, 
                                              n_channels=n_channels, 
                                              n_classes=n_classes, 
                                              nLayersForFeatExtr=nLayersForFeatExtr)
    
    # Re-use same instance of base network (weights are shared across both branches)
    processed_left_attn = attn_network(input_left)
    processed_right_attn = attn_network(input_right)
    
    
    merged_layer = Subtract()([processed_left_attn, processed_right_attn])
    merged_layer = Concatenate()([merged_layer, processed_left_attn, processed_right_attn])

    # Modules after L-1 merge
    x = Conv3D(n_channels, (3, 3, 3), padding='same')(merged_layer)
    x = BatchNormalization()(x)
    x = Activation('relu')(x)
    x = MaxPool3D(pool_size=(2, 2, 2))(x) 

    x = residual_block(x, input_channels=n_channels, output_channels=64)
    
    x = Flatten()(x)
    

    ####################### INCEPTION ########################################
    base_network = inception_CNN_2_5d(input_shape,
                                    depth=depth,
                                    activation=activation,
                                    use_dropout=use_dropout,
                                    use_pooling=use_pooling_before_merge,
                                    kernel_regularizer = kernel_regularizer,
                                    activity_regularizer = activity_regularizer)



    # Re-use same instance of base network (weights are shared across both branches)
    processed_left = base_network(input_left)
    processed_right = base_network(input_right)
    
    # Maxpooling
    if use_pooling_before_merge:
        processed_left = MaxPooling3D(strides=(2, 2, 2), padding='same')(processed_left)
        processed_right = MaxPooling3D(strides=(2, 2, 2), padding='same')(processed_right)

    #Subtract
    merged_layer = Subtract()([processed_left, processed_right])
    
    #Concatenate previous images
#     merged_layer = Concatenate()([merged_layer, processed_left, processed_right])
    
    filters = 2
#     kernel_regularizer = regularizers.l1_l2(l1=0.001, l2=0.001)
    kernel_regularizer = None
    
    # Inception module
    tower_1 = Conv3D(filters, kernel_size=(1, 1, 1), padding='same', activation=activation, kernel_regularizer=kernel_regularizer)(merged_layer)
    tower_2 = Conv3D(filters, kernel_size=(5, 5, 5), padding='same', activation=activation, kernel_regularizer=kernel_regularizer)(merged_layer)

    tower_3 = Conv3D(filters, kernel_size=(5, 5, 1), padding='same', activation=activation, kernel_regularizer=kernel_regularizer)(merged_layer)
    
    tower_4 = MaxPooling3D((3, 3, 3), strides=(1, 1, 1), padding='same')(merged_layer)
    tower_4 = Conv3D(filters, kernel_size=(1, 1, 1), padding='same', activation=activation, kernel_regularizer=kernel_regularizer)(tower_4)


    merged_layer = Concatenate(axis=4)([tower_1, tower_2, tower_4])

    
    if use_dropout:
        merged_layer = Dropout(0.05)(merged_layer)

    # Maxpooling
    if use_final_pooling:
        #Original
        merged_layer = MaxPooling3D(strides=(8, 8, 8), padding='same')(merged_layer)


    # Flatten
    merged_layer = Flatten()(merged_layer)
    merged_layer = Concatenate()([merged_layer, x])
    
    
    #Dropout
#     merged_layer = Dropout(0.05)(merged_layer)
    
    # Prediction layer
    prediction_ad = Dense(units=n_classes, activation='softmax')(merged_layer)
    prediction_mci = Dense(units=n_classes, activation='softmax')(merged_layer)


    return Model(inputs=[input_left, input_right], outputs=[prediction_ad, prediction_mci])


def LongitudinalAttention_CrossSectionalInception(input_shape,
                                        depth=2,
                                        n_inception=1, # # inception modules after merge
                                        activation='relu',
                                        use_dropout=False,
                                        use_pooling_before_merge=False,
                                        use_pooling_after_merge=False,
                                        use_final_pooling=True,
                                        n_classes=2,
                                        kernel_regularizer = None,
                                        activity_regularizer = None,
                                        n_channels=8,
                                        nLayersForFeatExtr=1):
    
    #Input
    input_left = Input(shape=(input_shape))
    input_right = Input(shape=(input_shape))    
    
    
    ####################### ATTENTION ########################################
    attn_network = residualAttentionNetwork(shape=input_shape, 
                                              n_channels=n_channels, 
                                              n_classes=n_classes, 
                                              nLayersForFeatExtr=nLayersForFeatExtr)
    
    # Re-use same instance of base network (weights are shared across both branches)
    processed_left_attn = attn_network(input_left)
    processed_right_attn = attn_network(input_right)

    merged_layer = Subtract()([processed_left_attn, processed_right_attn])
    merged_layer = Concatenate()([merged_layer, processed_left_attn, processed_right_attn])

    # Modules after L-1 merge
    x = Conv3D(n_channels, (3, 3, 3), padding='same')(merged_layer)
    x = BatchNormalization()(x)
    x = Activation('relu')(x)
    x = MaxPool3D(pool_size=(2, 2, 2))(x) 

    x = residual_block(x, input_channels=n_channels, output_channels=64)
    
    x = Flatten()(x)
    
    #Test 105
#     x = GlobalMaxPooling3D(data_format="channels_first")(x)
    

    ####################### INCEPTION ########################################
    base_network = inception_CNN_2_5d(input_shape,
                                    depth=depth,
                                    activation=activation,
                                    use_dropout=use_dropout,
                                    use_pooling=use_pooling_before_merge,
                                    kernel_regularizer = kernel_regularizer,
                                    activity_regularizer = activity_regularizer)



    # Re-use same instance of base network (weights are shared across both branches)
    processed_left = base_network(input_left)
    processed_right = base_network(input_right)
    
    # Maxpooling
    if use_pooling_before_merge:
        processed_left = MaxPooling3D(strides=(2, 2, 2), padding='same')(processed_left)
        processed_right = MaxPooling3D(strides=(2, 2, 2), padding='same')(processed_right)

    merged_layer = Concatenate()([processed_left, processed_right])
        
    # Maxpooling
    if use_final_pooling:
        
        #Test 92 (and many more)
        merged_layer = AveragePooling3D(strides=(4, 4, 4), padding='same')(merged_layer)


    # Flatten
    merged_layer = Flatten()(merged_layer)
    merged_layer = Concatenate()([merged_layer, x])
    
    
    # Prediction layer
    prediction = Dense(units=n_classes, activation='softmax')(merged_layer)


    return Model(inputs=[input_left, input_right], outputs=prediction)