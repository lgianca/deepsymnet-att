# Python file containing all parameters for deep networks
# Trust me, this is the best way to tweak parameters from one place!
# You can't possibly find all these parameters in the main code
# Don't even dare try!!!

import os
import random
import numpy as np
from keras import regularizers

# Set same seed for every run
randomNumberSeed = 12897
random.seed(randomNumberSeed)
np.random.seed(randomNumberSeed)

# Threshold for stroke volume
# All stroke samples with stroke volume less than this threshold are ignored
# stroke_threshold = 50

# These parameters are now used to resample data during pre-processing
# Parameters for data generation
# nPlanes = 20 # # planes in z direction
# scale = 0.2 # scale for resizing x-y planes
train_params = {'normalize': False,
          # 'resample_size': (int(146*scale), int(365*scale), nPlanes),
          'batch_size': 4,
          'n_classes': 2,
          'n_channels': 1,
          'shuffle': True}
test_params = {'normalize': False,
          # 'resample_size': (int(146*scale), int(365*scale), nPlanes),
          'batch_size': 1,
          'n_classes': 2,
          'n_channels': 1,
          'shuffle': False}



# dataType = 'saturated' # 'saturated' or 'vesselsRemoved'
# dataDir = os.path.join('data', 'brain', 'pre_processed', '_resizedToNetworkInput')
# xlsFilepath = os.path.join('res', 'stroke-cta-All2018-info.xlsx')

# dataDir = '/collab/dpena3/ADNI_longitudinal_MRI/network_data/cropped_empty_final_resized20_planes20'
# xlsFilepath = '/collab/dpena3/ADNI_longitudinal_MRI/ADNI_subject_to_BIDS_database_ml_final.csv'


# Whether to balance dataset imbalance
balanceDataset = False

# The following parameters are common to CNN based model or Inception based model
# Base network parameters
depth = 4 # # Layers in base network, supports only 2 or 4 for now
# 4 is best for now

useDropout = False # Whether to use dropout
activation = 'relu' # Activation function

# The following parameters are only needed for CNN based model
nConv = 1 # # Convolution layers at each depth
# 1 is best for now
nFilters = 16 # # filters in 1st hidden layer
# 16/32 is best for now
useRegularizers = False # Whether to use regularizers
if useRegularizers == True:
    kernel_regularizer = regularizers.l1_l2(l1=0.01, l2=0.01)
    activity_regularizer = regularizers.l1_l2(l1=0.01, l2=0.01)
else:
    kernel_regularizer = None
    activity_regularizer = None
batchNormalize = False # Whether to use batch normalization for filters
doubleFilters = False # Whether to double the number of filters at each layer
convAfterMerge = True
nLayersConvAfterMerge = 2
reduceDim = False # Whether to reduce the dimension of output features
# You must absolutely normalize outputs if you plan on using contrastive loss!
normalizeOutput = True # Normalize output using L2 normalization
# Whether to flatten output of base network
# False if using similarity Merge layer
# True if using subtract
flattenOutput = True
kernel = 5
kernel_size = (kernel, kernel, kernel)

# The following parameters are required only for inception based models
nInception = 2 # # inception modules after merge
usePoolingBeforeMerge = False
usePoolingAfterMerge = False
useFinalPooling = True
networkType = 'classification' # classification or regression
if networkType == 'regression':
    # Force stroke_threshold to 0
    stroke_threshold = 0

# The following parameters are used only for regression and multiclass
removeNoStroke = True

# The following parameters are used only for multiclass
multiclass = True # Whether to use multiclass model for classification
multiclass_loss = 'categorical_crossentropy'
multiclass_lr = 1e-7
n_classes_multiclass = 2
train_params_multiclass = {'normalize': False,
          # 'resample_size': (int(146*scale), int(365*scale), nPlanes),
          'batch_size': 4,
          'n_classes': n_classes_multiclass,
          'n_channels': 1,
          'shuffle': True}
test_params_multiclass = {'normalize': False,
          # 'resample_size': (int(146*scale), int(365*scale), nPlanes),
          'batch_size': 1,
          'n_classes': n_classes_multiclass,
          'n_channels': 1,
          'shuffle': False}

# Deprecated
# useInception = True
# if useInception:
#     # Hard set convAfterMerge to True
#     convAfterMerge = True
# if convAfterMerge:
#     # Hard set flattenOutput to False
#     flattenOutput = False



# Parameters for network training and model evaluation
nFolds = 10 # # folds for cross-validation
nEpochs = 40 # # epochs
optimizer = 'adam' # only adam and sgd are supported now
if networkType == 'classification':
    loss = 'binary_crossentropy'
    lr = 1e-6 # Learning rate
elif networkType == 'regression':
    loss = 'quantile' # 'logcosh' or 'mse' or 'quantile'
    if loss == 'quantile':
        q = 0.5
    lr = 1e-4

# Initial experiment mode, break after 1 fold of cross validation
initialExp = False
