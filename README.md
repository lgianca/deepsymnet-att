# deepsymnet-att
This repository contains supporting code for a manuscript that is in progress.

## Background and Goal
In this work, we propose a deep learning architecture that identifies longitudinal changes in subjects with Mild Cognitive Impairment (MCI) - subjects who are at high-risk of developing Alzheimer's Disease in the future. We integrated inception and attention modules in the architecture.

## Results
The final tuned DeepSymNet model performed comparable or better than existing Freesurfer longitudinal and voxel-based pipelines at a fraction of the time needed for preprocessing and predictions. Additionally, we showed that the DeepSymNet was able to generalize a neurodegenerative progression pattern when applied to an external set of high-risk patients known as Mild Cognitive Impairment. These results can be seen in the Jupyter Notebook. Further analysis indicated that the pallidum, putamen, and superior temporal gyrus regions were highly activated brain regions that helped drive the model's prediction. 

# Code

This code uses Python 3.7, conda environments, and Jupyter Notebook..

### Using conda only

To replicate environment, run:

```python 
conda env create -f deepsymnet-att.yml
```

That will install the necessary libraries, and activate the environment by running

```python
conda activate deepsymnet-att
```


## Files Included

1. Cross-Validation Model Weights
- model_weights (folder)
2. Model output results to help calculate area under the reciever operating curve 
- model_results (folder)
3. Example data to run through network
- example_brain (folder)
4. Jupyter Notebook & utilities to visualize and compute results
- utils.py
- results.ipynb

## Notes
An example application of our proposed model is shown at the end of the results.ipynb. An example brain was taken from http://www.neuroimagingprimers.org/examples/introduction-primer-example-boxes/, and the same brain was used for both sessions as input to the network.

# Alzheimer’s Disease Neuroimaging Initiative (ADNI)

In order to replicate the full analysis, you will have to request the ADNI dataset (http://adni.loni.usc.edu/). 

Data used in preparation of this article were obtained from the Alzheimer’s Disease Neuroimaging Initiative (ADNI) database (adni.loni.usc.edu). As such, the investigators within the ADNI contributed to the design and implementation of ADNI and/or provided data but did not participate in analysis or writing of this report. A complete listing of ADNI investigators can be found at: http://adni.loni.usc.edu/wp-content/uploads/how_to_apply/ADNI_Acknowledgement_List.pdf

# Other information
This code is free to use for any non-commercial purposes, provided that the original publication is cited. We refer to the original publication for additional information and acknowledgements.

